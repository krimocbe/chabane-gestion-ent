<?php

Auth::routes();
Route::group([ 'prefix' => 'dashboard','middleware' =>['auth']],function(){

  Route::get('/','DashboardController@index');
  Route::resource('achats', 'achatsController');
  Route::get('/cl','ClController@index')->name('cl');
  Route::post('/tests/imprimer_credit','CalculeController@imprimer');
  Route::get('/imprimer','VenteController@imprimer')->name('imprimer');
  Route::resource('produit','produitController');
  Route::get('/users','CalculeController@showtotal')->name('users');
  Route::resource('achatdiver','AchatdiverController');
  Route::get('/search','CalculeController@search');
  Route::resource('Vente','VenteController');
  Route::get('/rnt','CalculeController@zero');
  Route::resource('produit_vente','produit_venteController');
  Route::get('/putzero/{id}','CalculeController@puttozero')->name('putzero');
  Route::post('/tests/{id}/add_produit_divers','CalculeController@add_produit_divers');
  
  Route::get('/cherchez','VenteController@cherchez')->name('cherchez');
  Route::get('/putzer/{id}','CalculeController@puttozer')->name('putzero1');
  Route::get('/putze/{id}','CalculeController@puttozer1')->name('putzero2');

  Route::get('/vente_users','CalculeController@showtota')->name('vente_users');
  Route::get('/caisse','CalculeController@showcaisse')->name('caisse');
  Route::post('/tests/{test_id}/add_produit','CalculeController@add_divers');
  
  Route::get('/tests/{test_id}/get_produit','CalculeController@get_divers');
  
  Route::post('/tests/upd_produit','CalculeController@upd_produit');

  Route::post('/tests/{test_id}/upd_credit','CalculeController@upd_credit');
  
  Route::get('/tests/{test_id}/get_produit_divers','CalculeController@get_produit_divers');
  
  
  Route::get('/tests/{test_id}/get_achats','CalculeController@get_achats');
  Route::post('/tests/{test_id}/add_achats','CalculeController@add_achats');
  Route::post('/tests/upd_achats','CalculeController@upd_achats');
  

  
});  