import * as $ from 'jquery';
import 'bootstrap/dist/js/bootstrap.bundle.js'
export default (function () {
  // ------------------------------------------------------
  // @Popover
  // ------------------------------------------------------

  $('[data-toggle="popover"]').popover();

  // ------------------------------------------------------
  // @Tooltips
  // ------------------------------------------------------

  $('[data-toggle="tooltip"]').tooltip();
}());
