import React, { Component } from 'react';
export default class List extends Component {

  constructor (props) {
    super(props);
    this.state = {
      activeQuiz: null,
      i:0
    }
  }

render () {
    let { quizes , handleEdit} = this.props;
    return (
      <div className="row">
        <table className="table table-striped">
          <thead>
          <tr>
            <th>#</th>
            <th>Nom</th>
            <th>Mesure</th>
            <th>Prix_unitaire</th>
            <th>Quantité</th>
            <th>Total</th>
            <th>Actions</th>
          </tr>
          </thead>
          <tbody>
          {
            _.sortBy(quizes,'id').map((o) => {
              console.log(o);
              let i = this.state.i;

              if (o.name=="Mousse Molle") { let titre="Matelas en Mousse Molle";} 
                else if (o.name=="Mousse Demi-Dure") {let titre="Matelas en Mousse Demi-Dure";i=i+1;}

                else if (o.name=="Mousse Dure") {let titre="Matelas en Mousse Dure ";i=i+1;}

                else if (o.name=="Mousse Densité 30") {let titre="Matelas en Mousse Densité 30 "; i=i+1;}
              return (
                
                <tr key={o.id}>
                  <td>{o.id}</td>
                  <td>{o.name}</td>
                  <td>{o.mesure  || o.dimension}</td>
                  <td>{o.prix_unitaire}</td>
                  <td>{o.quantité}</td>
                  <td>{o.total}</td>
                  <td>
                    <button  className="btn btn-primary btn-xs" onClick={() => handleEdit(o)}> <i className="fa fa-pencil"></i></button>
                    <button  className="btn btn-danger btn-xs" onClick={() => this.props.handledelete(o.id)}><i className="fa fa-trash"></i></button>
                  </td>
                </tr>

              );
            })
          }
          </tbody>
        </table>

      </div>


    );
  }
}