import React, { Component } from 'react';
import ReactModal from 'react-modal';



const style = {
  overlay: {
    zIndex: 4000,
    position: 'fixed',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    overflow: 'auto',
    backgroundColor: 'rgba(0,0,0,0.4)'
  },
  content: {
    overflow: 'auto',
    WebkitOverflowScrolling: 'touch',
    backgroundClip: 'padding-box',
    backgroundColor: '#FFFFFF',
    border: '1px solid rgba(0, 0, 0, 0)',
    borderRadius: '4px',
    boxShadow: '0 1px 3px rgba(0, 0, 0, 0.3)',
    outline: '0 none',
    width: '50%',
    margin: '10% auto'
  }
};


const INITIAL_STATE = {
  id: -1,
  answer: '',
  points: 0,
  type: 'tto',
  case_sensitive: true,
  exercise_number: 0,
};

export default class TTO extends Component {
  constructor (props) {
    super(props);
    this.state = INITIAL_STATE;
  }

  handleClose () {
    this.setState(INITIAL_STATE);
    this.props.handleClose();
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.quiz) {
      this.setState({
        id: nextProps.quiz.id,
        answer: nextProps.quiz.answer,
        points: nextProps.quiz.points,
        case_sensitive: !!nextProps.quiz.case_sensitive,
        exercise_number: nextProps.quiz.exercise_number,
      })
    }
  }


  handleChange(field,e) {
    this.setState({ [field] : e.target.value })
  }

  render () {
    let {answer, option, points, case_sensitive,exercise_number} = this.state;
    let {addMCQ, modalShown, type , num_exercises} = this.props;
    console.log(num_exercises)
    return (
      <ReactModal
        isOpen={modalShown}
        contentLabel="Text a Trou"
        style={style}
        shouldCloseOnOverlayClick={true}
        className="modal-content"
      >
        <div className="modal-header">
          <h4 className="modal-title">Ajouter Text a Trou</h4>
          <button type="button" className="close" onClick={() => this.handleClose()}><span aria-hidden="true">×</span><span className="sr-only">Fermer</span></button>
        </div>
        <div className="modal-body">
          <div className="row">
            <div className="form-group col-md-4">
              <label htmlFor="">Reponse : </label>
              <input type="text" className="form-control" value={answer} onChange={(e) => this.handleChange('answer',e)}/>
            </div>
            <div className="form-group col-md-4">
              <label htmlFor="">Exercice : </label>
              <select name="" id="" className="form-control" value={exercise_number} onChange={(e) => this.handleChange('exercise_number',e)}>
                {_.range(num_exercises).map((v) => <option key={v} value={v}>Exercice {v+1}</option>)}
              </select>
            </div>
            <div className="form-group col-md-4">
              <label htmlFor="">Points : </label>
              <input type="number" className="form-control" value={points} onChange={(e) => this.handleChange('points',e)}/>
            </div>
          </div>
          <div className="form-check">
            <input className="form-check-input" type="checkbox" checked={case_sensitive}  onChange={() => this.setState({case_sensitive: !case_sensitive})} />
            <label className="form-check-label" htmlFor="defaultCheck1">Sensible à la casse</label>
          </div>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-default" onClick={(e) => this.props.handleClose()}>Annuler</button>
          <button type="button" className="btn btn-primary" onClick={() => this.props.handleSave(this.state)}>Sauvgarder</button>
        </div>
      </ReactModal>
    );
  }

}
