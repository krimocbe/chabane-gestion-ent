import React, { Component } from 'react';
import ReactModal from 'react-modal';

import Select  from 'react-select'
import 'react-select/dist/react-select.css';


const style = {
  overlay: {
    zIndex: 4000,
    position: 'fixed',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    overflow: 'auto',
    backgroundColor: 'rgba(0,0,0,0.4)'
  },
  content: {
    overflow: 'auto',
    WebkitOverflowScrolling: 'touch',
    backgroundClip: 'padding-box',
    backgroundColor: '#FFFFFF',
    border: '1px solid rgba(0, 0, 0, 0)',
    borderRadius: '4px',
    boxShadow: '0 1px 3px rgba(0, 0, 0, 0.3)',
    outline: '0 none',
    width: '50%',
    margin: '10% auto'
  }
};


const INITIAL_STATE = {
  type: 'rl',
  answer: '',
  points: 0,
  options: '',
  exercise_number: 0,

};

export default class RL extends Component {
  constructor (props) {
    super(props);
    this.state = INITIAL_STATE;
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.quiz && nextProps.quiz.type == "rl") {
      this.setState({
        id: nextProps.quiz.id,
        options: nextProps.quiz.options,
        exercise_number: nextProps.quiz.exercise_number,
      })
    }
  }

  handleClose () {
    this.setState(INITIAL_STATE);
    this.props.handleClose();
  }


  handleChange(field,e) {
    this.setState({ [field] : e.target.value })
  }

  render () {
    let {answer,options, points, value, exercise_number} = this.state;
    let {modalShown, type, num_exercises} = this.props;
    return (
      <ReactModal
        isOpen={modalShown}
        contentLabel="Reponse Libre"
        style={style}
        shouldCloseOnOverlayClick={true}
        className="modal-content"
      >
        <div className="modal-header">
          <h4 className="modal-title">Ajouter Reponse Libre</h4>
          <button type="button" className="close" onClick={() => this.handleClose()}><span aria-hidden="true">×</span><span className="sr-only">Fermer</span></button>
        </div>
        <div className="modal-body">
          <div className="form-group">
            <label htmlFor="">Exercice : </label>
            <select name="" id="" className="form-control" value={exercise_number} onChange={(e) => this.handleChange('exercise_number',e)}>
              {_.range(num_exercises).map((v) => <option key={v} value={v}>Exercice {v+1}</option>)}
            </select>
          </div>
          <div className="form-group">
            <label htmlFor="">Champ a afficher : </label>
            <textarea cols="30" rows="10" className="form-control" value={options} onChange={(e) => this.handleChange('options',e)} />
          </div>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-default" onClick={(e) => this.handleClose()}>Annuler</button>
          <button type="button" className="btn btn-primary" onClick={() => this.props.handleSave(this.state)}>Sauvgarder</button>
        </div>

      </ReactModal>
    );
  }

}
