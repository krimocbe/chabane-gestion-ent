import React, { Component } from 'react';
import LaddaButton, { S, SLIDE_UP } from 'react-ladda';

import TTO from './Modals/TTO';
import TTC from './Modals/TTC';
import RL from './Modals/RL';
import RLA from './Modals/RLA';
import QCM from './Modals/QCM';
import QuizList from './QuizList'
import { addQuiz , deleteQuiz, saveQuiz } from '../../actions/quiz';
import { loadTest, saveTest } from '../../actions/test';


export default class Test extends React.Component {

  constructor (props) {
    super(props);

    this.state = {
      testSaving: false,

      modalTTOShown: false,
      modalTTCShown: false,
      modalRLShown: false,
      modalRLAShown: false,
      modalQCMShown: false,

      test: null,
      quizes: [],
      activeQuiz: null
    };

  }

  componentDidMount () {

    loadTest(this.props.id).then(({data}) => {

      this.setState({
        test: data,
        quizes: data.quizes,
      });

    });
  }

  handleInput (key, e) {
    /*Duplicating and updating the state */
    this.setState({test: {...this.state.test, [key] : e.target.value }});
  }

  handleSaveQuiz(quiz) {
    if(quiz.id > 0) {
      saveQuiz(this.props.id,quiz).then(({data}) => {
        let quizes = _.filter(this.state.quizes,(q) => q.id !== data.id)
        this.setState({
          quizes: [ ...quizes,data],
          modalTTCShown: false ,
          modalTTOShown : false,
          modalRLShown: false,
          modalRLAShown: false,
          modalQCMShown: false
        })
      });
    } else {
      addQuiz(this.props.id,quiz).then(({data}) => {
            this.setState({ quizes: [...this.state.quizes, data],
             modalTTCShown: false ,
             modalTTOShown : false,
             modalRLShown: false,
             modalRLAShown: false,
             modalQCMShown: false
            })
      })
    }
  }

  handleDeleteQuiz(quiz_id) {

  }


  handleEdit(o) {
    let field = ''
    switch(o.type) {
      case 'tto':
        field = 'modalTTOShown'
        break;
      case 'ttc':
        field = 'modalTTCShown'
        break;
      case 'qcm':
        field = 'modalQCMShown'
        break;
      case 'rla':
        field = 'modalRLAShown'
        break;
      case 'rl':
        field = 'modalRLShown'
        break;
    }
    this.setState({ activeQuiz: o, [field] : true })
  }


  updateContent(content) {
    this.setState({content})
  }


  render () {
    let {
      test,
      quizes,
      activeQuiz,
      modalTTOShown,
      modalTTCShown,
      modalRLShown,
      modalRLAShown,
      modalQCMShown,
      testSaving
    } = this.state;

    if (!test) { return null; }
    return (
      <div>
        <div className="bgc-white p-20 bd">
          <div className="clearfix">
            <h6 className="c-grey-900 pull-left">Questions Dynamiques <small>(Sauvgarde automatique)</small></h6>
            <div className="dropdown pull-right">
              <button className="btn btn-link  dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i className="fa fa-plus"></i> Ajouter</button>
              <div className="dropdown-menu" aria-labelledby="dropdownMenuButton" x-placement="top-start">
                <a className="dropdown-item" onClick={() => this.setState({ modalTTOShown: true }) }>Text a trou</a>
                <a className="dropdown-item" onClick={() => this.setState({ modalTTCShown: true }) }>Text a trou avec choix</a>
                <a className="dropdown-item" onClick={() => this.setState({ modalQCMShown: true }) }>QCM</a>
                <a className="dropdown-item" onClick={() => this.setState({ modalRLAShown: true }) }>Reponse libre (auto corrigé)</a>
                <a className="dropdown-item" onClick={() => this.setState({ modalRLShown: true }) }>Reponse libre</a>
              </div>
            </div>
          </div>
          <div className="m-20">
            <QuizList quizes={quizes} handleEdit={(o) => this.handleEdit(o)} />
          </div>
        </div>

        <TTO modalShown={modalTTOShown} handleSave={this.handleSaveQuiz.bind(this)} quiz={activeQuiz} num_exercises={test.num_exercises}
                  handleClose={() => this.setState({modalTTOShown: false})} id={this.props.id}/>
        <TTC modalShown={modalTTCShown} handleSave={this.handleSaveQuiz.bind(this)} quiz={activeQuiz} num_exercises={test.num_exercises}
                  handleClose={() => this.setState({modalTTCShown: false})} id={this.props.id}/>
        <RL modalShown={modalRLShown} handleSave={this.handleSaveQuiz.bind(this)} quiz={activeQuiz} num_exercises={test.num_exercises}
                  handleClose={() => this.setState({modalRLShown: false})} id={this.props.id}/>
        <RLA modalShown={modalRLAShown} handleSave={this.handleSaveQuiz.bind(this)} quiz={activeQuiz} num_exercises={test.num_exercises}
                  handleClose={() => this.setState({modalRLAShown: false})} id={this.props.id}/>
        <QCM modalShown={modalQCMShown} handleSave={this.handleSaveQuiz.bind(this)}  quiz={activeQuiz} num_exercises={test.num_exercises}
                  handleClose={() => this.setState({modalQCMShown: false})} id={this.props.id}/>
      </div>

    );
  }
}
