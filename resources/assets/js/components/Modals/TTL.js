import React, { Component } from 'react';
import Modal from 'react-modal';
import Select  from 'react-select';
import 'react-select/dist/react-select.css';

const style = {
  overlay: {
    zIndex: 4000,
    position: 'fixed',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    overflow: 'auto',
    backgroundColor: 'rgba(0,0,0,0.4)'
  },
  content: {
    overflow: 'auto',
    WebkitOverflowScrolling: 'touch',
    backgroundClip: 'padding-box',
    backgroundColor: '#FFFFFF',
    border: '1px solid rgba(0, 0, 0, 0)',
    borderRadius: '4px',
    boxShadow: '0 1px 3px rgba(0, 0, 0, 0.3)',
    outline: '0 none',
    width: '50%',
    margin: '10% auto'
  }
};


export default class TTL extends Component {
  constructor (props) {
    super(props);

    this.closeModal = this.closeModal.bind(this);
    this.handleinput=this.handleinput.bind(this);

    this.state = {
      quiz:{
        id:0,
        dimension:"",
        name:"",
        prix_unitaire:0,
        quantité:0
      },
      modalShown:false

    };
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.quiz) {
      this.setState({
        quiz:nextProps.quiz
      });
    }
  }
  
  


  handleinput(field,e) {
    var state=Object.assign({},this.state.quiz);
    state[field]=e.target.value;
    this.setState({
      quiz:state
    });
  }

  handleOptionsChange(options) {
    this.setState({options})
  }

  closeModal()
  {
    this.props.handleClose();
  }

  

  render () {
    let modalShown=this.props.modalTTC;
    let id = this.state.quiz.id;
    let quiz= this.state.quiz;
    let mesure = this.state.quiz.dimension;
    let name= this.state.quiz.name;
    let prix_unitaire=this.state.quiz.prix_unitaire;
    let quantité=this.state.quiz.quantité;
    
    return (
            <Modal
              isOpen={modalShown}
              style={style}
              ariaHideApp={false}
              contentLabel="Example Modal"
              className="modal-content"
            >
    <div className="modal-header">
          <h4 className="modal-title">Ajouter Divers {id}  </h4>
          <button type="button" className="close" onClick={this.closeModal}><span aria-hidden="true">×</span><span className="sr-only">Fermer</span></button>
    </div>
             
          <h6 className="c-grey-900">Test</h6>
          <div className="m-20">
            <div className="form-row">
              <div className="form-group col-md-6">
                <label htmlfor="inputNom">Nom</label>
                <input id="inputNom" value={name} type="text" onChange={(e)=>this.handleinput('name',e)}  className="form-control" name="name" />
              </div>
              <div className="form-group col-md-6">
                <label htmlfor="inputExercises">Mesure</label>
                <input id="inputExercises" value={mesure} type="text" name="dimension" onChange={(e)=>this.handleinput('dimension',e)}  className="form-control"/>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6">
                <label htmlfor="inputNom">prix_unitaire</label>
                <input id="inputNom" value={prix_unitaire} type="text" onChange={(e)=>this.handleinput('prix_unitaire',e)} className="form-control" name="prix_unitaire" />
              </div>
              <div className="form-group col-md-6">
                <label htmlfor="inputExercises">quantité</label>
                <input id="inputExercises" value={quantité} type="number" onChange={(e)=>this.handleinput('quantité',e)} name="quantite"  className="form-control"/>
              </div>
            </div>
            <div className="row justify-content-end">
              <div className="col-md-3 d-flex justify-content-end">
                <button className="btn btn-primary btn-lg"  onClick={(e)=>this.props.edit(quiz)}><i className="ti-save"></i> Sauvegarder</button>
              </div>
            </div>
            </div>
      </Modal>
    );
  }

}
