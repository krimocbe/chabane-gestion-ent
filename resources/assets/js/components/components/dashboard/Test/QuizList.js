import React, { Component } from 'react';
export default class QuizList extends Component {

  constructor (props) {
    super(props);
    this.state = {
      activeQuiz: null
    }
  }

  render () {
    let { quizes , activeQuiz , handleEdit} = this.props
    return (
      <div className="row">
        <table className="table table-striped">
          <thead>
          <tr>
            <th>#</th>
            <th>Type</th>
            <th>Exercice</th>
            <th>Points</th>
            <th>Actions</th>
          </tr>
          </thead>
          <tbody>
          {
            _.sortBy(quizes,'id').map((o) => {
              return (
                <tr key={o.id}>
                  <td>{o.id}</td>
                  <td>{o.type.toUpperCase()}</td>
                  <td>Exercice {o.exercise_number+1}</td>
                  <td>{o.points || '/'}</td>
                  <td>
                    <button  className="btn btn-primary btn-xs" onClick={() => handleEdit(o)}> <i className="fa fa-pencil"></i></button>
                    <button disabled className="btn btn-danger btn-xs" onClick={() => this.props.handledelete(o.id)}><i className="fa fa-trash"></i></button>
                  </td>
                </tr>
              );
            })
          }
          </tbody>
        </table>

      </div>


    );
  }
}
