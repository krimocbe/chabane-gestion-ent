import React, { Component } from 'react';
import ReactModal from 'react-modal';
import deepcopy from 'deepcopy';


const style = {
  overlay: {
    zIndex: 4000,
    position: 'fixed',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    overflow: 'auto',
    backgroundColor: 'rgba(0,0,0,0.4)'
  },
  content: {
    overflow: 'auto',
    WebkitOverflowScrolling: 'touch',
    backgroundClip: 'padding-box',
    backgroundColor: '#FFFFFF',
    border: '1px solid rgba(0, 0, 0, 0)',
    borderRadius: '4px',
    boxShadow: '0 1px 3px rgba(0, 0, 0, 0.3)',
    outline: '0 none',
    width: '50%',
    margin: '10% auto'
  }
};


const INITIAL_STATE = {
  id: -1,
  dimension: '',
  prix_unitaire: 0,
  type: 'tto',
  quantite: true,
  total: 0,
};

export default class TTO extends Component {
  constructor (props) {
    super(props);
    this.state = deepcopy(INITIAL_STATE);
  }
  
  handleClose () {
    this.setState(deepcopy(INITIAL_STATE));
    this.props.handleClose();
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.quiz) {
      this.setState({
        id: nextProps.quiz.id,
        answer: nextProps.quiz.answer,
        points: nextProps.quiz.points,
        case_sensitive: !!nextProps.quiz.case_sensitive,
        exercise_number: nextProps.quiz.exercise_number,
      })
    }
  }


  handleChange(field,e) {
    this.setState({ [field] : e.target.value })
  }

  render () {
    let {  id,
  dimension,
  prix_unitaire,
  type,
  quantite,
  total
} = this.state;
    let {addMCQ, modalShown, type , num_exercises} = this.props;
    console.log(num_exercises)
    return (
      <ReactModal
        isOpen={modalShown}
        contentLabel="Text a Trou"
        style={style}
        shouldCloseOnOverlayClick={true}
        className="modal-content"
      >
        <div className="modal-header">
          <h4 className="modal-title">Mousse molle</h4>
          <button type="button" className="close" onClick={() => this.handleClose()}><span aria-hidden="true">×</span><span className="sr-only">Fermer</span></button>
        </div>
        <div className="modal-body">
          <div className="row">
            <div className="form-group col-md-4">
              <label htmlFor="">Dimension</label>
            <select name="" id="" className="form-control" value={dimension} onChange={(e) => this.handleChange('dimension',e)}>
              </select>
            </div>

            <div className="form-group col-md-4">
              <label htmlFor="">prix_unitaire</label>
            <select name="" id="" className="form-control" value={prix_unitaire} onChange={(e) => this.handleChange('prix_unitaire',e)}>
              </select>
            </div>
       
          <div className="form-group col-md-4">
              <label htmlFor="">quantite : </label>
              <input type="number" className="form-control" value={quantite} onChange={(e) => this.handleChange('quantite',e)}/>
            </div>
          </div>
         </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-default" onClick={(e) => this.props.handleClose()}>Annuler</button>
          <button type="button" className="btn btn-primary" onClick={() => this.props.handleSave(this.state)}>Sauvgarder</button>
        </div>
      </ReactModal>
    );
  }

}
