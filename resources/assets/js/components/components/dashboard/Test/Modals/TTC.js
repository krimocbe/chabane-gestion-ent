import React, { Component } from 'react';
import ReactModal from 'react-modal';
import deepcopy from 'deepcopy';
import Select  from 'react-select'
import 'react-select/dist/react-select.css';


const style = {
  overlay: {
    zIndex: 4000,
    position: 'fixed',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    overflow: 'auto',
    backgroundColor: 'rgba(0,0,0,0.4)'
  },
  content: {
    overflow: 'auto',
    WebkitOverflowScrolling: 'touch',
    backgroundClip: 'padding-box',
    backgroundColor: '#FFFFFF',
    border: '1px solid rgba(0, 0, 0, 0)',
    borderRadius: '4px',
    boxShadow: '0 1px 3px rgba(0, 0, 0, 0.3)',
    outline: '0 none',
    width: '50%',
    margin: '10% auto'
  }
};


const INITIAL_STATE = {
  type: 'ttc',
  answer: '',
  points: 0,
  value: null,
  options: [],
  exercise_number: 0,

};

export default class TTC extends Component {
  constructor (props) {
    super(props);
    this.state = deepcopy(INITIAL_STATE);
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.quiz && nextProps.quiz.type == "ttc") {
      this.setState({
        id: nextProps.quiz.id,
        answer: {
          label: nextProps.quiz.answer,
          value: nextProps.quiz.value,
        },
        points: nextProps.quiz.points,
        options: JSON.parse(nextProps.quiz.options),
        case_sensitive: !!nextProps.quiz.case_sensitive,
        exercise_number: nextProps.quiz.exercise_number,
      })
    }
  }

  handleClose () {
    this.setState(deepcopy(INITIAL_STATE));
    this.props.handleClose();
  }


  handleChange(field,e) {
    this.setState({ [field] : e.target.value })
  }

  handleOptionsChange(options) {
    this.setState({options})
  }

  render () {
    let {answer,options, points, value , exercise_number} = this.state;
    let {addMCQ, modalShown, type , num_exercises} = this.props;
    return (
      <ReactModal
        isOpen={modalShown}
        contentLabel="Text a Trou avec choix"
        style={style}
        shouldCloseOnOverlayClick={true}
        className="modal-content"
      >
        <div className="modal-header">
          <h4 className="modal-title">Ajouter Text a Trou avec choix</h4>
          <button type="button" className="close" onClick={() => this.handleClose()}><span aria-hidden="true">×</span><span className="sr-only">Fermer</span></button>
        </div>
        <div className="modal-body">
          <div className="form-group">
            <label htmlFor="">Options : </label>
            <Select.Creatable
              multi={true}
              onChange={this.handleOptionsChange.bind(this)}
              value={options}
            />
          </div>

          <div className="row">

            <div className="form-group col-md-4">
              <label htmlFor="">ReponseKFJKGHJLFGJH : </label>
              <Select value={answer} options={options} onChange={(answer) => this.setState({answer})} />
            </div>
            <div className="form-group col-md-4">
              <label htmlFor="">Exercice : </label>
              <select name="" id="" className="form-control" value={exercise_number} onChange={(e) => this.handleChange('exercise_number',e)}>
                {_.range(num_exercises).map((v) => <option key={v} value={v}>Exercice {v+1}</option>)}
              </select>
            </div>
            <div className="form-group col-md-4">
              <label htmlFor="">Points : </label>
              <input type="number" className="form-control" value={points} onChange={(e) => this.handleChange('points',e)}/>
            </div>
          </div>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-default" onClick={(e) => this.props.handleClose()}>Annuler</button>
          <button type="button" className="btn btn-primary" onClick={() => this.props.handleSave(this.state)}>Sauvgarder</button>
        </div>

      </ReactModal>
    );
  }

}
