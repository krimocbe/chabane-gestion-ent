import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Modal from 'react-modal';
import List from './List';
import {addproduit_divers} from './actions/produit';
import {load_Products}from './actions/produit';
import TTC from './Modals/TTC';
import {updateproduct}from './actions/produit';
import {updatecredit}from './actions/produit';
import {imprimeproduct}from './actions/produit';
import bootstrap from '../bootstrap';
import Simplelert from 'react-simplert';
import SweetAlert from 'sweetalert2-react';


  const style = {
    overlay: {
      zIndex: 4000,
      position: 'fixed',
      left: 0,
      top: 0,
      width: '100%',
      height: '100%',
      overflow: 'auto',
      backgroundColor: 'rgba(0,0,0,0.4)'
    },
    content: {
      overflow: 'auto',
      WebkitOverflowScrolling: 'touch',
      backgroundClip: 'padding-box',
      backgroundColor: '#FFFFFF',
      border: '1px solid rgba(0, 0, 0, 0)',
      borderRadius: '4px',
      boxShadow: '0 1px 3px rgba(0, 0, 0, 0.3)',
      outline: '0 none',
      width: '80%',
      margin: '10% auto'
    }
  };
  
    

export default class Divers extends Component {
    constructor() {
        super();
        this.add= this.add.bind(this);
        this.edit= this.edit.bind(this);
        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.handleinput=this.handleinput.bind(this);
        this.handleinput1=this.handleinput1.bind(this);
        this.calcule=this.calcule.bind(this);
        this.imprimer=this.imprimer.bind(this);
      
        this.state = {
          modalIsOpen: false,
          id:document.getElementById('divers').getAttribute('data-id'),
          list: [],
          divers:{
            name:"",
            mesure:"",
            quantite:"",
            prix_unitaire:""
          },
          credit:{
          },
          total:0,
          tranche:0,
          modalTTC: false,
          activeQuiz: null,
          restant:0 ,
          showSimplert:false,
          type:"Success",
          title:"",
          message:"",
          show: false,
          msg:"",
          type1:"achat"
          
        };
      }
      

      openModal() {
        this.setState({modalIsOpen: true});
      }

      edit(quiz){
        console.log("home");
        console.log(quiz);
        updateproduct(quiz).then((data)=>{
          console.log(data.data);
          this.setState({
          list:data.data,
          modalTTC:false  
        });
        let total=0;     
        this.state.list.map((ligne)=>{
          total=total+ligne.prix_unitaire*ligne.quantité
        });
        console.log(total);
        this.setState({
          total:total
        });  

      });
    }
      
    handleinput(key,e){
      var state=Object.assign({},this.state.divers);
      state[key]=e.target.value;
      this.setState({
       divers:state
      });
    }
       
    handleinput1(key,e){
      this.setState({
        tranche:e.target.value
      });
    }
    handleEdit(o) {
      let field = 'modalTTC';
      this.setState({ activeQuiz: o, [field] : true });
      console.log("hhhhhhhhhhhh");
      console.log(this.state.modalTTC);
    }

    add()
    {
    console.log("helllo");
      console.log(this.state.id);        
      var state=Object.assign({},this.state.divers);
      addproduit_divers(this.state.id,state).then((data)=>{
        console.log(data.data[0]);
        this.setState({ 
          modalIsOpen: false,
          list: [ ...this.state.list,data.data[0]],
          total:data.data[1]
        });
      });
/*    loadProducts(this.state.id).then(({data}) => {
      let total=0;
      console.log("go again",data[0]);
      data[0].map((ligne)=>{
      console.log(ligne.prix_unitaire);      
      total=total+ligne.prix_unitaire*ligne.quantité
      });
      this.setState({
        total:total,
        list: data[0],
        credit:data[1]
      });


    });*/
    }
  
  imprimer(divID){
      var headstr = "<html><head><title></title></head><body>";
      var footstr = "</body>";
      var newstr = document.all.item(divID).innerHTML;
      var oldstr = document.body.innerHTML;
      document.body.innerHTML = headstr+newstr+footstr;
      window.print();
      document.body.innerHTML = oldstr;
      return true;
  }




  afterOpenModal() {
        // references are now sync'd and can be accessed.
      //  this.subtitle.style.color = '#f00';
  }
    
  closeModal() {
        this.setState({modalIsOpen: false});
  }

  handledelete(id){
    console.log(id);
  }
      

  componentDidMount () {
    load_Products(this.state.id).then(({data}) => {
      let total=0;
      console.log("go again",data[0]);
      data[0].map((ligne)=>{
      console.log(ligne.prix_unitaire);      
      total=total+ligne.prix_unitaire*ligne.quantité
      });
      this.setState({
        total:total,
        list: data[0],
        credit:data[1]
      });
    });
  }

  
  handleSaveQuiz(quiz) {
  }

  calcule(){
    let total=0;
    this.state.list.map((ligne)=>{
          total=total+ligne.prix_unitaire*ligne.quantité
    });
    this.setState({
      total:total
    });
    let restant=0;
    this.setState({
      caisse:this.state.tranche
    });
    if(this.state.tranche<this.state.total)
    {
      let new_total=this.state.total-this.state.tranche;
      restant=new_total+this.state.credit.montant_credit;
      this.setState({
        restant:restant
      }); 
      console.log("*********************************************");
      console.log(this.state.restant);
      console.log(restant);
    }else {
      let new_total=this.state.tranche-this.state.total;
      restant=this.state.credit.montant_credit-new_total;
      this.setState({
        restant:restant
      });
      console.log("*********************************************");
      console.log("state restant",this.state.restant);
      console.log("restant",restant);
    }
    let id=this.state.credit.id;
    console.log("state restant",this.state.restant);
    console.log("restant",restant);
    updatecredit(this.state,id).then(({data}) => {
      console.log("*********************************************");
      console.log(data);
       var msg= "Votre Nouveau crédit est : "+data;
       this.setState({
        
            showSimplert:true,
            type:"Success",
            title:"Credit",
            message:data,
            show: true,
            msg:msg
        });
      
      console.log("**",data);
    });

      
  }
    
    render() {
      let id= this.state.id;
      let list= this.state.list;
      let activeQuiz= this.state.activeQuiz;
      let modalTTC=this.state.modalTTC;
      var total=this.state.total;
      console.log('total',total);
      let credit=this.state.credit.montant_credit;
      let restant=this.state.restant;
      let show = this.state.show;
      let tranche = this.state.tranche;
    
    return (
    <div>
        <SweetAlert
            type='success'
            show={show}
            title="Succés"
            animation = "false"
            customClass='animated tada'
            text={this.state.msg}
            onConfirm={() => this.setState({ show: false })}
        />

            <div className="m-20">
              <List quizes={list} handleEdit={(o) => this.handleEdit(o)}  handledelete={(id) =>{console.log("fuck0");}} />
            </div>
      
            <Modal
              isOpen={this.state.modalIsOpen}
              onAfterOpen={this.afterOpenModal}
              onRequestClose={this.closeModal}
              style={style}
              ariaHideApp={false}
              contentLabel="Example Modal"
              className="modal-content"
            >
    <div className="modal-header">
          <h4 className="modal-title">Ajouter Divers {id} </h4>
          <button type="button" className="close" onClick={this.closeModal}><span aria-hidden="true">×</span><span className="sr-only">Fermer</span></button>
    </div>
             
          <h6 className="c-grey-900">Test</h6>
          <div className="m-20">
            <div className="form-row">
              <div className="form-group col-md-6">
                <label for="inputNom">Nom</label>
                <input id="inputNom" type="text" onChange={(e)=>this.handleinput('name',e)}  className="form-control" name="name" />
              </div>
              
              <div className="form-group col-md-6">
                <label for="inputExercises">Mesure</label>
                <input id="inputExercises" type="text" name="mesure" onChange={(e)=>this.handleinput('mesure',e)}  className="form-control"/>
              </div>
            </div>
            
            <div className="form-row">
              <div className="form-group col-md-6">
                <label for="inputNom">prix_unitaire</label>
                <input id="inputNom" type="text" onChange={(e)=>this.handleinput('prix_unitaire',e)} className="form-control" name="prix_unitaire" />
              </div>
              <div className="form-group col-md-6">
                <label for="inputExercises">quantité</label>
                <input id="inputExercises" type="number" onChange={(e)=>this.handleinput('quantite',e)} name="quantite"  className="form-control"/>
              </div>
            </div>
            
            <div className="row justify-content-end">
              <div className="col-md-3 d-flex justify-content-end">
                <button className="btn btn-primary btn-lg"  onClick={(e)=>this.add()}><i className="ti-save"></i> Sauvegarder</button>
              </div>
            </div>
            </div>
      </Modal>
   

 
<div className="row gap-20">
    <div className="col-md-3">
        <div className="layers bd bgc-white p-20">
            <div className="layer w-100 mB-10">
                <h6 className="lh-1">Total </h6>
            </div>
            <div className="layer w-100">
                <div className="peers ai-sb fxw-nw">
                    <div className="peer peer-greed">
                        <span id="sparklinedash"></span>
                    </div>
                    <div className="peer">
                        <span className="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-green-50 c-green-500">{ total } </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div className="col-md-3">
        <div className="layers bd bgc-white p-20">
            <div className="layer w-100 mB-10">
                <h6 className="lh-1">Crédit </h6>
            </div>
            <div className="layer w-100">
                <div className="peers ai-sb fxw-nw">
                    <div className="peer peer-greed">
                        <span id="sparklinedash3"></span>
                    </div>
                    <div className="peer">
                        <span className="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-blue-50 c-blue-500">{credit} </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div className="col-md-3">
        <div className="layers bd bgc-white p-20">
            <div className="layer w-100 mB-10">
                <h6 className="lh-1">Versement</h6>
            </div>
            <div className="layer w-50">
                <div className="peers ai-sb fxw-nw">
                    <div className="peer peer-greed">
                        <span id="sparklinedash4"></span>
                    </div>
                    <div className="peer"> <input type="number" onChange={(e)=>this.handleinput1('tranche',e)}  name="net"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <div className="col-md-3">
        <div className="layers bd bgc-white p-20">
            <div className="layer w-100 mB-10">
                <h6 className="lh-1"> Nouveau crédit </h6>
            </div>
            <div className="layer w-100">
                <div className="peers ai-sb fxw-nw">
                    <div className="peer peer-greed">
                        <span id="sparklinedash3"></span>
                    </div>
                    <div className="peer">
                        <span className="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-blue-50 c-blue-500">{restant} </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
   

    
</div>
      
    
            

    <TTC modalTTC={modalTTC}  quiz={activeQuiz} edit={(quiz)=>this.edit(quiz)} 
                  handleClose={()=>this.setState({modalTTC: false})} />
   
    <Simplelert
            showSimplert={ this.state.showAlert }
            type={ this.state.typeAlert }
            title={ this.state.titleAlert }
            message={ this.state.messageAlert }
    />
<div className="row gap-20">
    <div className="col-md-2">  
      <button onClick={this.openModal} className="btn btn-primary btn-lg">Ajouter divers</button>
    </div>
    <div className="col-md-3">  
      <button className="btn btn-primary btn-lg"  onClick={(e)=>this.calcule()}><i class="ti-save"></i> Calculer Nouveau credit</button>
    </div>
    <div className="col-md-4">  
      <button className="btn btn-primary btn-lg"  onClick={(e)=>this.imprimer('divID')}><i class="ti-save"></i> Imprimer</button>
    </div>
</div>
<div id="divID" style={{ marginTop: `80px` }}>
<h1>Bon de livraison ETC dehag </h1>
<div className="row">
        <table className="table table-striped">
          <thead>
          <tr>
            <th>#</th>
            <th>Designation/Nom</th>
            <th>Mesure</th>
            <th>Prix_unitaire</th>
            <th>Quantité</th>
            <th>Total</th>
            
          </tr>
          </thead>
          <tbody>
          {
            _.sortBy(list,'id').map((o) => {
              console.log(o);
              let i = this.state.i;

              if (o.quantité==0) { return;} 
                else if (o.name=="Mousse Demi-Dure") {let titre="Matelas en Mousse Demi-Dure";i=i+1;}

                else if (o.name=="Mousse Dure") {let titre="Matelas en Mousse Dure ";i=i+1;}

                else if (o.name=="Mousse Densité 30") {let titre="Matelas en Mousse Densité 30 "; i=i+1;}
              return (
                
                <tr key={o.id}>
                  <td>{o.id}</td>
                  <td>{o.name}</td>
                  <td>{o.mesure  || o.dimension}</td>
                  <td>{o.prix_unitaire}</td>
                  <td>{o.quantité}</td>
                  <td>{o.total}</td>
                </tr>

              );
            })
          }
          </tbody>
        </table>

      </div>


---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
<div className="row gap-20">
    <div className="col-md-3">
        <div className="layers bd bgc-white p-20">
            <div className="layer w-100 mB-10">
                <h6 className="lh-1">Total </h6>
            </div>
            <div className="layer w-100">
                <div className="peers ai-sb fxw-nw">
                    <div className="peer peer-greed">
                        <span id="sparklinedash"></span>
                    </div>
                    <div className="peer">
                        <span className="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-green-50 c-green-500">{ total } </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div className="col-md-3">
        <div className="layers bd bgc-white p-20">
            <div className="layer w-100 mB-10">
                <h6 className="lh-1">Crédit </h6>
            </div>
            <div className="layer w-100">
                <div className="peers ai-sb fxw-nw">
                    <div className="peer peer-greed">
                        <span id="sparklinedash3"></span>
                    </div>
                    <div className="peer">
                        <span className="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-blue-50 c-blue-500">{credit} </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div className="col-md-3">
        <div className="layers bd bgc-white p-20">
            <div className="layer w-100 mB-10">
                <h6 className="lh-1">Versement</h6>
            </div>
            <div className="layer w-50">
                <div className="peers ai-sb fxw-nw">
                    <div className="peer peer-greed">
                        <span id="sparklinedash4"></span>
                    </div>
                    <div className="peer">
                        <span className="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-blue-50 c-blue-500">{tranche} </span>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
     <div className="col-md-3">
        <div className="layers bd bgc-white p-20">
            <div className="layer w-100 mB-10">
                <h6 className="lh-1"> Nouveau crédit </h6>
            </div>
            <div className="layer w-100">
                <div className="peers ai-sb fxw-nw">
                    <div className="peer peer-greed">
                        <span id="sparklinedash3"></span>
                    </div>
                    <div className="peer">
                        <span className="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-blue-50 c-blue-500">{restant} </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
   

    
</div>
</div>

</div>      


          
        );
      }

}

if (document.getElementById('divers')) {
    
    console.log('founed');
    ReactDOM.render(<Divers/>, document.getElementById('divers'));
}   
