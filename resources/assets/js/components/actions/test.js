import axios from 'axios';

export const loadTest = (id) =>
  axios({
    method: 'GET',
    url: `/dashboard/tests/${id}`
  });


export const saveTest = (id, test) =>
  axios({
    method: 'PUT',
    url: `/dashboard/tests/${id}`,
    data: test
  });

