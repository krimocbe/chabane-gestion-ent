import axios from 'axios';

export const addproduit = (test_id,quiz) =>
  axios({
    method: 'POST',
    url: `/dashboard/tests/${test_id}/add_produit`,
    data: quiz
  });


 export const loadProducts=(test_id)=>
 	axios({
 		method:'GET',
 		url:`/dashboard/tests/${test_id}/get_produit`

 	});
 export const load_Products=(test_id)=>
 	axios({
 		method:'GET',
 		url:`/dashboard/tests/${test_id}/get_produit_divers`
	}); 

 export const updateproduct=(quiz)=>
    axios({
    	method:'POST',
    	url: `/dashboard/tests/upd_produit`,
    	data:quiz
    });

 export const updatecredit=(credit,id)=>
 	axios({
 		method:'POST',
    	url: `/dashboard/tests/${id}/upd_credit`,
    	data:credit
    }) 

 export const imprimeproduct=(credit)=>
 	axios({
 		method:'POST',
 		url: `/dashboard/tests/imprimer_credit`,
    	data:credit
	});

 export const addproduit_divers=(test_id,quiz)=>
 	axios({
 		method: 'POST',
    	url: `/dashboard/tests/${test_id}/add_produit_divers`,
    	data: quiz
	});	      	