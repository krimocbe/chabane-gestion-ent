import axios from 'axios';

export const addachat = (test_id,quiz) =>
  axios({
    method: 'POST',
    url: `/dashboard/tests/${test_id}/add_achats`,
    data: quiz
  });


 export const loadachats=(test_id)=>
 	axios({
 		method:'GET',
 		url:`/dashboard/tests/${test_id}/get_achats`
});


 

 export const updatecredit=(credit,id,tranche)=>
 	axios({
 		method:'POST',
    	url: `/dashboard/tests/${id}/upd_credit`,
    	data:credit
    }) 

 export const imprimeachat=(credit)=>
 	axios({
 		method:'POST',
 		url: `/dashboard/tests/imprimer_credit`,
    	data:credit
	});

 export const updateachat=(quiz)=>
    axios({
        method:'POST',
        url: `/dashboard/tests/upd_achats`,
        data:quiz
    });   	