
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require('./bootstrap');


require('./components/Counter');

require('./components/Divers');

require('./components/Achat');


const Swal = require('sweetalert2');
  
function warnBeforeRedirect(linkURL) {
    /*Swal({
      title: "Leave this site?", 
      text: "If you click 'OK', you will be redirected to " + linkURL, 
      type: "warning",
      showCancelButton: true
    }.then(function(isConfirm){

    console.log(isConfirm);
   if (isConfirm){
     swal("Shortlisted!", "Candidates are successfully shortlisted!", "success");

    } else {
      swal("Cancelled", "Your imaginary file is safe :)", "error");
         e.preventDefault();
    }
 })

    /*linkURL="http://localhost:8000/dashboard/achats";
    window.location.href = linkURL;*/
    Swal({
          title: "Voulez vous enregistrez et quitter ?", 
          text: "", 
          type: "warning",
          showCancelButton: true
        }).then(function(isConfirm) {
          if (isConfirm) {
            Swal({
              title: 'Shortlisted!',
              text: 'Vos transactions sont Sauvegardées!',
              icon: 'success'
            }).then(function() {
                window.location.href = linkURL;    
            });
          } else {
            Swal("Annulé", "Your imaginary file is safe :)", "error");
          }
        });  

  }

$('#false').click(function(e) {
    e.preventDefault(); // Prevent the href from redirecting directly
    var linkURL = $(this).attr("href");
    console.log(linkURL);
    warnBeforeRedirect(linkURL);
  });

  

$('#inputNom').on('keyup',function(){
 
            $value=$(this).val();
            $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
            $.ajax({
 
                        type : 'get',
                        url : '/dashboard/cherchez',
                        data:{
                            'search':$value
                        },
                        success:function(data){
 
                            console.log(data);
                            $('tbody').html(data);
 
                        }
 
            });
 })


$('#search1').on('keyup',function(){
 
            $value=$(this).val();
            $value2=$('#search2').val();
            $value3=$('#search3').val();
            $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
            $.ajax({
 
                        type : 'get',
                        url : '/dashboard/search',
                        data:{
                            'search':$value,
                            'search2':$value2,
                            'search3':$value3,
                        },
                        success:function(data){
 
                            console.log(data);
                            $('tbody').html(data);
 
                        }
 
            });
 })
 
 


$('#search2').on('keyup',function(){
 
            $value=$('#search1').val();
            $value2=$(this).val();
            $value3=$('#search3').val();
            
            console.log($value);
 
            $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
            $.ajax({
 
                        type : 'get',
 
                        url : '/dashboard/search',
 
                        data:{
                            'search':$value,
                            'search2':$value2,
                            'search3':$value3,
                        },
                        success:function(data){
 
                            console.log(data);
                            $('tbody').html(data);
 
                        }
 
            });
 })
 
$('#search3').on('keyup',function(){
 
            $value=$('#search1').val();
            $value2=$('#search2').val();
            $value3=$(this).val();
 
            $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
            $.ajax({
 
                        type : 'get',
 
                        url : '/dashboard/search',
 
                        data:{
                            'search':$value,
                            'search2':$value2,
                            'search3':$value3,
                        }
                        ,
                        success:function(data){
 
                            console.log(data);
                            $('tbody').html(data);
 
                        }
 
            });
 })
 