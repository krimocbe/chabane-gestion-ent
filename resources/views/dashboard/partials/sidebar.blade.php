<div class="sidebar">
    <div class="sidebar-inner">
        <!-- ### $Sidebar Header ### -->
        <div class="sidebar-logo">
            <div class="peers ai-c fxw-nw">
                <div class="peer peer-greed">
                    <a class='sidebar-link td-n' href="/dashboard" class="td-n">
                        <div class="peers ai-c fxw-nw">
                            <div class="peer">
                                <div class="logo">
                                    <img src="/images/gva_logo_small.png" style="width: 70%; height: auto; padding: 10px" alt="">
                                </div>
                            </div>
                            <div class="peer peer-greed">
                                <h5 class="lh-1 mB-0 logo-text">Vge</h5>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="peer">
                    <div class="mobile-toggle sidebar-toggle">
                        <a href="" class="td-n">
                            <i class="ti-arrow-circle-left"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <!-- ### $Sidebar Menu ### -->
        <ul class="sidebar-menu scrollable pos-r">
            <li class="nav-item mT-15 active">
                <a class='sidebar-link' href="/dashboard" default>
					<span class="icon-holder">
						<i class="c-blue-500 ti-home"></i>
					</span>
                    <span class="title">Dashboard</span>
                </a>
            </li>
            <li class="nav-item mT-15 ">
                <a class='sidebar-link' href="{{ route('achats.index') }}" default>
					<span class="icon-holder">
						<i class="c-red-500 ti-file"></i>
					</span>
                    <span class="title">Achats</span>
                </a>

            </li>
            <li class="nav-item mT-15 ">
                <a class='sidebar-link' href="{{ route('achatdiver.index') }}" default>
                    <span class="icon-holder">
                        <i class="c-red-500 ti-file"></i>
                    </span>
                    <span class="title">Achats divers</span>
                </a>

            </li>
            <li class="nav-item mT-15 ">
                <a class='sidebar-link' href="{{ route('users') }}" default>
                    <span class="icon-holder">
                        <i class="c-red-500 ti-file"></i>
                    </span>
                    <span class="title">Consulter les Transaction</span>
                </a>

            </li>
            <li class="nav-item mT-15 ">
                <a class='sidebar-link' href="{{ route('Vente.index') }}" default>
					<span class="icon-holder">
						<i class="c-green-500 ti-user"></i>
					</span>
                    <span class="title">Vente</span>
                </a>
            </li>

            <li class="nav-item mT-15 ">
                <a class='sidebar-link' href="{{ route('vente_users') }}" default>
                    <span class="icon-holder">
                        <i class="c-green-500 ti-user"></i>
                    </span>
                    <span class="title">Statistiques</span>
                </a>
            </li>

            <li class="nav-item mT-15 ">
                <a class='sidebar-link' href="{{ route('caisse') }}" default>
                    <span class="icon-holder">
                        <i class="c-blue-500 ti-volume"></i>
                    </span>
                    <span class="title">caisse</span>
                </a>
            </li>

            <li class="nav-item mT-15 ">
                <a class='sidebar-link' href="{{ route('cl') }}" default>
                    <span class="icon-holder">
                        <i class="c-blue-500 ti-volume"></i>
                    </span>
                    <span class="title">état des créances</span>
                </a>
            </li>

        </ul>
    </div>
</div>
