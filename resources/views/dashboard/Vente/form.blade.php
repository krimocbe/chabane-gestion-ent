<div class="row mB-40">
  <div class="col-sm-8">
    <div class="bgc-white p-20 bd">

        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', '',['class' => 'form-control']) }}

        {{ Form::label('adresse', 'adresse') }}
        {{ Form::text('adresse' , '', ['class' => 'form-control']) }}

        {{ Form::label('tel', 'tel') }}
        {{ Form::text('tel' , '', ['class' => 'form-control']) }}
        
        {{ Form::label('type', 'type') }}
        {{ Form::select('type' ,  array('Normal' => 'Normal', 'particulier' => 'particulier') ,  null, ['class' => 'form-control'] ) }} 
    </div>
  </div>
</div>

