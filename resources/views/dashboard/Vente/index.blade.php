@extends('dashboard.layouts.main')

@section('title')
    Vente
@endsection

@section('content')

    <div class="d-flex mB-30">
        <h4 class="mr-auto c-grey-900">
        <span class="icon-holder">
            <i class="c-red-500 ti-file"></i>
        </span>
            Vente
        </h4>

        <a href="{{ route('Vente.create') }} " class="btn btn-primary"><i class="fa fa-plus"></i> Ajouter un client</a>
              
    </div>
    <div class="d-flex mB-30">
             
              <div class="form-group col-md-6">
                <label for="inputNom"> cherchez un Nom du client</label>
                <input id="inputNom" type="text"  class="form-control" name="name" />
              </div>
    </div>
    <table class="table">
        <thead class="thead-light">
        <tr>
            
            <th>nom client</th>
            <th>Adresse</th>
            <th>Tel</th>
            <th>action</th>
        </tr>
        </thead>

        <tbody>
        
        
        @foreach($tests as $test)
            <tr>
                <td>{{ $test->name}}</td>
                <td>{{ $test->adresse }}</td>
                <td>{{ $test->Tel }}</td>
                <td>
                    <a href="{{ route('Vente.edit',$test->id) }}" class="btn btn-primary btn-xs"><i
                                class="fa fa-pencil"></i></a>
                    {!! Form::open(['method' => 'DELETE','route' => ['Vente.destroy', $test->id],'style'=>'display:inline']) !!}

                    <button type="submit" class="btn btn-danger btn-xs cur-p"><i class="fa fa-trash"></i></button>

                    {!! Form::close() !!}
                </td>
                
            </tr>
        @endforeach
        


        </tbody>
      
            <script src="{{ asset('js/app.js') }}"></script>

    </table>
@endsection

