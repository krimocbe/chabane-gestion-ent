@extends('dashboard.layouts.main')

@section('page-header')
    Tests <small>{{ trans('app.update_item') }}</small>
@endsection

@section('content')

<div>
        {!! Form::model($test, ['method' => 'PATCH','route' => ['produit.update', $test->id ]]) !!}
        <div class="bgc-white p-20 mB-20 bd">
          <h6 class="c-grey-900">Test</h6>

          <div class="m-20">

            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="inputNom">Designation</label>
                <input id="inputNom" type="text" value="{{ $test->name }}" class="form-control" name="name" />
              </div>
              <div class="form-group col-md-6">
                <label for="inputExercises">Dimension</label>
                <input id="inputExercises" type="text" name="mesure" value="{{ $test->dimension }}" class="form-control"/>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="inputNom">prix_unitaire</label>
                <input id="inputNom" type="text" value="{{ $test->prix_unitaire }}" class="form-control" name="prix_unitaire" />
              </div>
              <div class="form-group col-md-6">
                <label for="inputExercises">quantité</label>
                <input id="inputExercises" type="number" name="quantité" value="{{ $test->quantité }}"  class="form-control"/>
              </div>
            </div>

            


            <div class="row justify-content-end">
              <div class="col-md-3 d-flex justify-content-end">
                <button class="btn btn-primary btn-lg"><i class="ti-save"></i> Sauvegarder</button>
              </div>
            </div>
          </div>
        </div>
        {!! Form::close() !!}

@endsection
