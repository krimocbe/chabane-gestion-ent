@extends('dashboard.layouts.main')

@section('title')
    Achats
@endsection

@section('content')

    <div class="d-flex mB-30">
        <h4 class="mr-auto c-grey-900">
        <span class="icon-holder">
            <i class="c-red-500 ti-file"></i>
        </span>
            Achats-divers
        </h4>
        
    </div>
    <table class="table">
        <thead class="thead-light">
        <tr>
            
            <th>nom fournisseur</th>
            <th>Prenom fournisseur</th>
            <th>Tel</th>
            <th>action</th>
        </tr>
        </thead>

        <tbody>
        
        
        @foreach($tests as $test)
            <tr>
                <td>{{ $test->name}}</td>
                <td>{{ $test->prenom }}</td>
                <td>{{ $test->Tel }}</td>
                <td>
                    <a href="{{ route('achatdiver.edit',$test->id) }}" class="btn btn-primary btn-xs"><i
                                class="fa fa-pencil"></i></a>
                    {!! Form::open(['method' => 'DELETE','route' => ['achats.destroy', $test->id],'style'=>'display:inline']) !!}

                    <button type="submit" class="btn btn-danger btn-xs cur-p"><i class="fa fa-trash"></i></button>

                    {!! Form::close() !!}
                </td>
                
            </tr>
        @endforeach
        


        </tbody>
      
            <script src="{{ asset('js/app.js') }}"></script>

    </table>
@endsection

