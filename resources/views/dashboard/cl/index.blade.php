@extends('dashboard.layouts.main')

@section('title')
    Etats des créance
@endsection

@section('content')
<script language="javascript" type="text/javascript">
    function printdiv(divID)
    {
      var headstr = "<html><head><title></title></head><body>";
      var footstr = "</body>";
      var newstr = document.all.item(divID).innerHTML;
      var oldstr = document.body.innerHTML;
      document.body.innerHTML = headstr+newstr+footstr;
      window.print();
      document.body.innerHTML = oldstr;
      return false;
    }
</script>
<div id="divID">

    <div class="d-flex mB-30">
        <h4 class="mr-auto c-grey-900">
        <span class="icon-holder">
            <i class="c-red-500 ti-file"></i>
        </span>
            Etats des créance   
         
        </h4>
        <input name="b_print" type="button" onclick="printdiv('divID');" value=" Print " class="btn btn-primary" />

    </div>
    <hr>
<div class="row gap-20">
    <div class="col-md-3">
        <div class="layers bd bgc-white p-20">
            <div class="layer w-100 mB-10">
                <h6 class="lh-1">Total crédit</h6>
            </div>
            <div class="layer w-100">
                <div class="peers ai-sb fxw-nw">
                    <div class="peer peer-greed">
                        <span id="sparklinedash"></span>
                    </div>
                    <div class="peer">
                        <span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-green-50 c-green-500">{{  $sum_credit }} </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="layers bd bgc-white p-20">
            <div class="layer w-100 mB-10">
                <h6 class="lh-1">crédit initial  </h6>
            </div>
            <div class="layer w-100">
                <div class="peers ai-sb fxw-nw">
                    <div class="peer peer-greed">
                        <span id="sparklinedash4"></span>
                    </div>
                    <div class="peer">
                        <span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-green-50 c-green-500">{{  $progression }} </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="layers bd bgc-white p-20">
            <div class="layer w-100 mB-10">
                <h6 class="lh-1">progression  </h6>
            </div>
            <div class="layer w-100">
                <div class="peers ai-sb fxw-nw">
                    <div class="peer peer-greed">
                        <span id="sparklinedash4"></span>
                    </div>
                    <div class="peer">
                        <span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-green-50 c-green-500">{{ $sum_credit-$progression }} </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="layers bd bgc-white p-20">
            <div class="layer w-100 mB-10">
                <h6 class="lh-1">Progression </h6>
            </div>
            <div class="layer w-100">
                <div class="peers ai-sb fxw-nw">
                    <div class="peer peer-greed">
                        <span id="sparklinedash2"></span>
                    </div>
                    <div class="peer">
                        <span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-green-50 c-green-500">   {{ $sum_credit-$progression }}
                @if($sum_credit-$progression==0)
                     =================  
                 @elseif($sum_credit-$progression>0)
                       <i class="fa fa-arrow-up" style="font-size:10px;color:red"></i>
                 @elseif($sum_credit-$progression<0)
                       <i class="fa fa-arrow-down" style="font-size:10px;color:green"></i>
                @endif       
                  </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>
    <table class="table">
        <thead class="thead-light">
        <tr>
            
            <th>nom </th>
            <th>Type </th>
            <th>Tel</th>
            <th>Crédit</th>
            <th>Date derniére opération</th>
            <th>crédit initial </th>
            <th>progression </th>
            <th>Etat du progression </th>
            
            
        </tr>
        </thead>

        <tbody>
        
        
        @foreach($userData as $credits)
            <tr>
                <td>{{  $credits['cr']['fournisseur']['name']   OR   $credits['cr']['client']['name'] }}</td>
                <td>{{  $credits['cr']['type'] }}  </td>
                <td>{{  $credits['cr']['fournisseur']['Tel']   OR   $credits['cr']['client']['Tel'] }}</td>
                <td>{{  $credits['cr']['montant_credit'] }}</td>
                <td> @if(isset($credits['date']['created_at'])) {{  $credits['date']['created_at'] }}  @endif </td>

                <td>{{ $credits['cr']['progression'] }}</td>
                <td>{{  $credits['cr']['montant_credit']-$credits['cr']['progression'] }}</td>
                <td>
                    {{ $credits['cr']['montant_credit']-$credits['cr']['progression'] }}
                @if($credits['cr']['montant_credit']-$credits['cr']['progression']==0)
                     =================  
                 @elseif($credits['cr']['montant_credit']-$credits['cr']['progression']>0)
                       <i class="fa fa-arrow-up" style="font-size:30px;color:red"></i>
                 @elseif($credits['cr']['montant_credit']-$credits['cr']['progression']<0)
                       <i class="fa fa-arrow-down" style="font-size:30px;color:green"></i>
                        
                 @endif 
                  </td>
                
            </tr>
        @endforeach
        


        </tbody>
      
            <script src="{{ asset('js/app.js') }}"></script>

    </table>
 </div>
           
@endsection

