@extends('dashboard.layouts.main')

@section('title')
    Caisse
@endsection

@section('content')

    <div class="d-flex mB-30">
        <h4 class="mr-auto c-grey-900">
        <span class="icon-holder">
            <i class="c-red-500 ti-file"></i>
        </span>
            Caisse du {{ NOW()->format('d-m-Y') }} 
        </h4>
    </div>
    <table class="table">
        <thead class="thead-light">
        <tr>
            
            <th>Date Aujourd'hui</th>
            <th>revenu des achats</th>
            <th>revenu des ventes</th>
            <th>Caisse d'hier</th>
            <th>Caisse D'aujourdhui</th>
        </tr>
        </thead>

    <tbody>
            <tr>
                <td>{{NOW()->format('d-m-Y') }}</td>
                <td>{{ $caisse->total_achat }}</td>
                <td>{{ $caisse->total_vente}}</td>
                <td>{{ $caisse->derniere_caisse }} </td> 
                <td>{{ $caisse->total_vente+$caisse->derniere_caisse-$caisse->total_achat }} </td>
            </tr>
    </tbody>
      
            <script src="{{ asset('js/app.js') }}"></script>

    </table>
@endsection
