@extends('dashboard.layouts.main')

@section('title')
    Achats
@endsection

@section('content')

    <div class="d-flex mB-30">
        <h4 class="mr-auto c-grey-900">
        <span class="icon-holder">
            <i class="c-red-500 ti-file"></i>
        </span>
            Achats
        </h4>
        <a href="{{ route('achats.create') }} " class="btn btn-primary"><i class="fa fa-plus"></i> Ajouter un fournisseur</a>
    </div>
    <table class="table">
        <thead class="thead-light">
        <tr>
            
            <th>nom fournisseur</th>
            <th>Prenom fournisseur</th>
            <th>Tel</th>
            <th>action</th>
        </tr>
        </thead>

        <tbody>
        
        
        @foreach($test as $tests)
            <tr>
                <td>{{ $tests->name}}</td>
                <td>{{ $tests->prenom }}</td>
                <td>{{ $tests->Tel }}</td>
                <td>
                    <a href="{{ route('achats.edit',$tests->id) }}" class="btn btn-primary btn-xs"><i
                                class="fa fa-pencil"></i></a>
                    {!! Form::open(['method' => 'DELETE','route' => ['achats.destroy', $tests->id],'style'=>'display:inline']) !!}

                    <button type="submit" class="btn btn-danger btn-xs cur-p"><i class="fa fa-trash"></i></button>

                    {!! Form::close() !!}
                </td>
                
            </tr>
        @endforeach
        


        </tbody>
      
            <script src="{{ asset('js/app.js') }}"></script>

    </table>
@endsection

