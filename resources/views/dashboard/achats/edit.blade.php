@extends('dashboard.layouts.main')

@section('page-header')
    Tests <small></small>
@endsection

@section('content')
  
            <script src="{{ asset('js/app.js') }}"></script>
 
    <div class="d-flex mB-30">
        <h4 class="mr-auto c-grey-900">
        <span class="icon-holder">
            <i class="c-red-500 ti-file"></i>
        </span>
            Achats
        </h4>
        <a href="{{ route('putzero', $test->id) }} " id="false" class="btn btn-primary"><i class="fa fa-plus"></i> Enregistrer   </a>
    </div>
  


<div id="counter" data-id="{{ $test->id }}" ></div>

<script type="text/javascript">
function myFunction()
{
   var x;
   var r=confirm("Are you sure you want to leave this page ?");
   if (r)
   {
      window.location="{{URL::to('putzero', $test->id)}}";
   }
}   
</script>


@endsection
@section('js')




@endsection


