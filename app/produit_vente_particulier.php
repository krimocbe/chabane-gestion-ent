<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class produit_vente_particulier extends Model
{
    //
    protected $table = 'produit_vente_particulier';
	protected $fillable = [
        'name', 'dimension', 'prix_unitaire','quantité','total','dispo','client_id','transaction_id','type'
    ];
}
