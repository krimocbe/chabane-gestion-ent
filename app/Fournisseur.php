<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fournisseur extends Model
{
    //
    //
        protected $fillable = [
        'name', 'tel', 'prenom',
    ];

     public function produits()
    {
        return $this->belongsToMany('App\produit');
    }

    public function credit()
    {
    	return $this->hasOne('App\credit');
    }
}
