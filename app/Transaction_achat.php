<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction_achat extends Model
{
    //
    protected $table = 'transaction-achats';
	protected $fillable = [
        'name', 'mesure', 'prix_unitaire','quantité','total','fournisseur_id','date_add','produit_id','type'
    ];
    public function fournisseur()
    {
        return $this->belongsTo('App\fournisseur');
    }
}
