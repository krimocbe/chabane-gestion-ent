<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Caisse extends Model
{
        protected $table = 'caisse';
        protected $fillable = [
        'total_vente', 'total_achat', 'total_achat_divers','caisse','date_caisse','derniere_caisse'
    ];      
}
