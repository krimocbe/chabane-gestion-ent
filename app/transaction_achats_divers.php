<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transaction_achats_divers extends Model
{
    //
    
     protected $table = 'transaction-achats-divers';
	protected $fillable = [
        'name',  'prix_unitaire','quantité','total','fournisseur_id','type','date_add','mesure','produit_id'
    ];
    public function fournisseur()
    {
        return $this->belongsTo('App\fournisseur');
    }
}
