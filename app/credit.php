<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class credit extends Model
{
    //
        protected $table = 'credits';
        protected $fillable = [
        'montant_credit', 'id_fournisseur', 'id_client','type','progression','tour'
    ];

    public function fournisseur()
    {
    	return $this->belongsto('App\fournisseur','id_fournisseur');
    }

     public function client()
    {
    	return $this->belongsto('App\client','id_client');
    }
}
