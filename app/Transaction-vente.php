<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction_vente extends Model
{
    //
    protected $table = 'transaction_vente';
	protected $fillable = [
        'name', 'dimension', 'prix_unitaire','quantité','total','dispo','client_id','date_add','transaction_id'
    ];

    public function client()
    {
        return $this->belongsTo('App\Client');
    }
}
