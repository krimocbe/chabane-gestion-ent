<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class produit_vente extends Model
{
    //

    protected $table = 'produit_vente';
	protected $fillable = [
        'name', 'dimension', 'prix_unitaire','quantité','total','dispo','client_id','transaction_id','type'
    ];


}
