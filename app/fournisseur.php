<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class fournisseur extends Model
{
    //
      protected $table = 'fournisseur';
        protected $fillable = [
        'name', 'tel', 'prenom',
    ];

     public function produits()
    {
        return $this->hasMany('App\produit');
    }

    public function credit()
    {
    	return $this->hasOne('App\credit','id_fournisseur');
    }

     public function Transaction_achats()
    {
        return $this->hasMany('App\Transaction_achat','fournisseur_id');
    }

     public function Transaction_achats_divers()
    {
        return $this->hasMany('App\Transaction_achats_divers','fournisseur_id');
    }
    

}
