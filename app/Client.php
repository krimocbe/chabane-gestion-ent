<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //
        protected $table = 'client';
        protected $fillable = [
        'name', 'tel', 'adresse','type'
    ];

   public function credit()
    {
    	return $this->hasOne('App\credit','id_client');
    }

     public function Transaction_ventes()
    {
        return $this->hasMany('App\Transaction_vente','client_id');
    }
}
