<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class produit extends Model
{
    //
    protected $table = 'produit';
	protected $fillable = [
        'name', 'mesure', 'prix_unitaire','quantité','total','fournisseur_id','type','transaction_id'
    ];

     public function fournisseurs()
    {
        return $this->belongsToMany(Fournisseur::class);
    }


}
