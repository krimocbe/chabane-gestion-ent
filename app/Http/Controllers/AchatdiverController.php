<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\fournisseur;
use App\produit;
use View;
use App\transaction_achats_divers;
use App\Transaction_achat;
use App\Transaction_vente;


class AchatdiverController extends Controller
{
    //
      public function index()
    {
        $tests = fournisseur::all();
        if (request()->wantsJson()) {
            return response()->json($tests);
                                    }
        return View::make('dashboard.achatsdivers.index')->with('tests', $tests);;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        
        return view('dashboard.achatsdivers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        //$this->validate($request,fournisseur::rules());
        $total = $request->quantité*$request->prix_unitaire;
        $date=NOW()->format('Y-m-d');
        transaction_achats_divers::create([
         	'prix_unitaire'=> $request->prix_unitaire,
        	'quantité'=> $request->quantité,
        	'total'=> $total,
        	'type'=> 1,
        	'name'=> $request->name,
        	'date_add'=> $date,
        	'fournisseur_id'=>$request->id,
            'mesure'=>$request->mesure
		
        ]);

        Transaction_achat::create([
        	'prix_unitaire'=> $request->prix_unitaire,
        	'quantité'=> $request->quantité,
        	'total'=> $total,
        	'mesure'=> "/",
        	'name'=> $request->name,
        	'fournisseur_id'=>$request->id,
        	'type'=>"achat_divers",
        	'date_add'=> $date,
        	
		]);
        
        $test = fournisseur::find($request->id);
        $produit = transaction_achats_divers::all()->where('type','=',1)->where('afficher','=',0);
        $total_prix= $produit->sum('total');
		return view('dashboard.achatsdivers.edit',compact('test','produit','total_prix'));
     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    /*public function show($id)
    {
        return Test::with('quizes')->findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $produit = fournisseur::find($id);
        $test = transaction_achats_divers::find($id);
		//$total_prix= $produit->sum('total');


        return view('dashboard.achatsdivers.edit',compact('test','produit','total_prix'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,Test $test)
    {
        $this->validate($request,fournisseur::rules(true));
        $test->update($request->all());
        return redirect()->back()->withSuccess(trans('app.success_update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {

        fournisseur::destroy($id);

        return redirect()->route('tests.index');
    }  

    public function show($id)
    {
    	    $test=Transaction_vente::find($id);
            return view('dashboard.achatsdivers.editproduit',compact('test'));
    }
}
