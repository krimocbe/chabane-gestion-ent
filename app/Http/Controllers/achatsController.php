<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use App\fournisseur;
use App\produit;
use App\credit;
use Illuminate\Support\Facades\Redirect;

class achatsController extends Controller
{
    //
    public function index()
    {
        // load the view and pass the tests

        $tests = fournisseur::all();
        if (request()->wantsJson()) {
            return response()->json($tests);
                                    }
        return View::make('dashboard.achats.index')->with('test', $tests);;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('dashboard.achats.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $test = fournisseur::create(($request->all()));
        $credit=credit::create([
                'type'=>"fournisseur",
                'montant_credit'=>0,
                'id_fournisseur'=>$test->id,
                'progression'=>0,
                'tour'=>0
        ]);
        $montant=$credit->montant_credit;
        $id=$test->id;
        $produit = produit::all();
        $total_prix= produit::all()->sum('total');
        $tests=fournisseur::all();
        return redirect()->route('achats.index'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    /*public function show($id)
    {
        return Test::with('quizes')->findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $test = fournisseur::find($id);
        $produit = produit::all();
        $total_prix= produit::all()->sum('total');
        $credit=credit::whereId_fournisseur($test->id)->first();
        $montant =$credit->montant_credit; 
        return view('dashboard.achats.edit',compact('test','produit','total_prix','montant'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,Test $test)
    {
        dd($request->all());
        $this->validate($request,fournisseur::rules(true));
        $test->update($request->all());
        return redirect()->back()->withSuccess(trans('app.success_update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {

        fournisseur::destroy($id);
        return redirect()->route('achats.index'); 
    
        
    }  
}
 