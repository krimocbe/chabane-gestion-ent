<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction_achat;
use App\Transaction_vente;
use App\produit;
use App\fournisseur;
use App\Client;
use App\produit_vente;
use App\transaction_achats_divers;
use App\Caisse;
use App\credit;
use App\produit_vente_particulier;
use View;
use PDF;
use Khill\Lavacharts\Lavacharts;

class CalculeController extends Controller
{
    //

    public function showtotal()
    {
         $tests=Transaction_achat::all();
         return View::make('dashboard.op.index')->with('tests', $tests);
    
	}
	
	public function add_divers(Request $request,$id)
	{
		$date=NOW()->format('Y-m-d');
        $total=$request->quantite * $request->prix_unitaire;
        $trans=transaction_achats_divers::create([
            'prix_unitaire'=> $request->prix_unitaire,
            'quantité'=> $request->quantite,
            'total'=> $total,
            'type'=> 1,
            'name'=> $request->name,
            'date_add'=> $date,
            'fournisseur_id'=>$id,
            'mesure'=>$request->mesure,
            'produit_id'=>0
        ]);
        $date=NOW()->format('Y-m-d');
        $produit=produit::create([
            'name'=> $request->name,
            'mesure'=> $request->mesure,
            'prix_unitaire'=> $request->prix_unitaire,
            'quantité'=> $request->quantite,
            'total'=>$total,
             'fournisseur_id'=>$id,
            'transaction_id'=>$trans->id,
            'type'=>"achat_divers"
        ]);
        
        $total= produit::all()->sum('total');
        return response()->json([$produit,$total]);
   
	}
    
    public function upd_credit(Request $request,$id)
    {
        
        $date=NOW()->format('Y-m-d');
        $credit=credit::whereId($id)->first();
        if ($credit->tour==0) {
            $credit->update([
            "montant_credit"=>$request->restant,
            "tour"=>$request->tour+1,
            "progression"=>$request->restant
            ]);
        }else
        $credit->update([
            "montant_credit"=>$request->restant,
            "tour"=>$request->tour+1
        ]);
        $last_caisse=Caisse::where('date_caisse', '=' , $date)
           ->first();
        if($last_caisse==null) {
               $last_caisse=Caisse::orderBy('date_caisse', 'desc')->first();
               if($last_caisse==null) $derniere_caisse=0;
                    else $derniere_caisse=$last_caisse->caisse;  
               Caisse::create([
                    'total_vente'=>$request->caisse,
                    'total_achat'=>0,
                    'total_achat_divers'=>0,
                    'caisse'=>0,
                    'derniere_caisse'=>$derniere_caisse,
                    'date_caisse'=>$date
               ]);
         }else{
            $last_caisse=Caisse::where( 'date_caisse', '=' , $date)->first();
            if($request->type1=="vente")
            {   $total=$last_caisse->total_vente + $request->tranche;
                $last_caisse->update([
                    'total_vente'=> $total 
                ]);
            }
            else if ($request->type1=="achat") 
             {

            $total=$last_caisse->total_achat + $request->caisse;
            $last_caisse->update([
               'total_achat'=> $total 
            ]);
            } 
            
         }   
        
        return response()->json($credit->montant_credit);
   }
    
    public function upd_produit(Request $request)
    {
      $date=NOW()->format('Y-m-d');
      $last_caisse=Caisse::where( 'date_caisse', '=' , $date)
           ->first();
        
     if(($request->type=="achat_divers_divers")){
        $trans=transaction_achats_divers::whereId($request->transaction_id)->first();
        $trans->update([
            'prix_unitaire'=> $request->prix_unitaire,
            'quantité'=> $request->quantité,
            'total'=> $request->prix_unitaire*$request->quantité,
            'mesure'=> $request->mesure,
            'name'=> $request->name,
            'date_add'=> $date
        ]);
     } 
    else if(($request->type=="achat_divers")){
        $date=NOW()->format('Y-m-d');
        $trans=transaction_achats_divers::whereId($request->transaction_id)->first();
        $trans->update([
            'prix_unitaire'=> $request->prix_unitaire,
            'quantité'=> $request->quantité,
            'total'=> $request->prix_unitaire*$request->quantité,
            'mesure'=> $request->mesure,
            'name'=> $request->name,
            'date_add'=> $date
        ]);
     }else if ($request->transaction_id==0) {
        $date=NOW()->format('Y-m-d');
        $trans=Transaction_achat::create([
            'prix_unitaire'=> $request->prix_unitaire,
            'quantité'=> $request->quantité,
            'total'=> $request->prix_unitaire*$request->quantité,
            'mesure'=> $request->mesure,
            'name'=> $request->name,
            'fournisseur_id'=>$request->fournisseur_id,
            'type'=>$request->type,
            'date_add'=> $date,
            'produit_id'=>$request->id
        ]);
        }else{
        $date=NOW()->format('Y-m-d');
        $trans=transaction_achat::whereId($request->transaction_id)->first();
        $trans->update([
            'prix_unitaire'=> $request->prix_unitaire,
            'quantité'=> $request->quantité,
            'total'=> $request->prix_unitaire*$request->quantité,
            'mesure'=> $request->mesure,
            'name'=> $request->name,
            'date_add'=> $date
        ]);
        }
      $produit1=produit::find($request->id);
      $produit1->update([
          "name" => $request->name,
          "mesure" => $request->mesure,
          "prix_unitaire" => $request->prix_unitaire,
          "quantité" => $request->quantité,
          "total"=>$request->prix_unitaire*$request->quantité,
          "transaction_id"=>$trans->id
      ]);
      $produit=produit::all();
      return response()->json($produit);

    }
    public function get_produit_divers($id){

        $produit=produit::whereType('achat_divers_divers')->get();
        $test = fournisseur::find($id);
        $credit=credit::whereId_fournisseur($test->id)->first();
        return response()->json([$produit,$credit],200);

    }
    public function get_divers($id)
    {
        $produit=produit::all();
        $produit = $produit->unique();
        $test = fournisseur::find($id);
        $credit=credit::whereId_fournisseur($test->id)->first();
        return response()->json([$produit,$credit],200);
    }



    public function puttozerr(Request $request)
    {

        $test = fournisseur::find($request->id);
        transaction_achats_divers::where('afficher', '=', 0)->update([
    		'afficher' => 1
    	]);
        $produit = transaction_achats_divers::all()->where('type','=',1)->where('afficher','=',0);
        $total_prix= $produit->sum('total');
        return redirect()->route('achatdiver.index');
   
   //	return view('dashboard.achatsdivers.edit',compact('test','produit','total_prix'));

    }



    public function showtota()
    {
         $tests=Transaction_vente::all()->sum('total');
         
         $lava = new Lavacharts; // See note below for Laravel

        $finances = $lava->DataTable();

        $finances->addDateColumn('Year')
                ->addNumberColumn('Vente')
                ->addNumberColumn('Achats')
                ->addNumberColumn('Net Worth')
                ->addRow(['2009-1-1', $tests, 490, 1324])
                ->addRow(['2010-1-1', 1000, 400, 1524])
                ->addRow(['2011-1-1', 1400, 450, 1351])
                ->addRow(['2012-1-1', 1250, 600, 1243])
                ->addRow(['2013-1-1', 1100, 550, 1462]);

        $lava->ComboChart('Finances', $finances, [
            'title' => 'Company Performance',
            'titleTextStyle' => [
                'color'    => 'rgb(123, 65, 89)',
                'fontSize' => 16
            ],
            'legend' => [
                'position' => 'in'
            ],
            'seriesType' => 'bars',
            'series' => [
                2 => ['type' => 'line']
            ]
        ]);
        $lava1 = new Lavacharts; // See note below for Laravel

        $reasons = $lava1->DataTable();

        $reasons->addStringColumn('Reasons')
             ->addNumberColumn('Percent')
             ->addRow(['Check Reviews', 5])
             ->addRow(['Watch Trailers', 2])
             ->addRow(['See Actors Other Work', 4])
             ->addRow(['Settle Argument', 89]);

        $lava1->DonutChart('IMDB', $reasons, [
            'title' => 'Reasons I visit IMDB'
        ]);
         return View::make('dashboard.op.affichage')->with('lava', $lava,'lava1',$lava1);
    
    }

    public function zero(Request $request)
    {
			if($request->ajax())
 
		{
 
			$output="";
 			$products=Transaction_vente::where('name','LIKE','%'.$request->search."%")->get();
 			if($products)
 			{
 
				foreach ($products as $key => $product) {
 
				$output.='<tr>'.
 
				'<td>'.$product->name.'</td>'.
 
				'<td>'.$product->prix_unitaire.'</td>'.
 
				'<td>'.$product->quantité.'</td>'.
 
				'<td>'.$product->dimension.'</td>'.

				'<td>'.$product->dispo.'</td>'.

           
 
				'</tr>';
 
			}
        	return Response($output);
 			}
 
		}
    	
    }


    public function search(Request $request)
	{
 
		if($request->ajax())
        {
 
			$post1=$request->search2;
            if($post1=="Achat"){
                $output="";
                $products=Transaction_achat::all();
                $products1=transaction_achats_divers::all();
                foreach ($products as $product) {
                    $fournisseur=$product->fournisseur;
                    $product->push($fournisseur);
                }
                foreach ($products1 as $productb) {
                    $fournisseur=$productb->fournisseur;
                    $productb->push($fournisseur);
                } 

            $all_products = $products->concat($products1);
            $post=$request->search;
            $post2=$request->search3;
            $collection = collect([]);
            if(($post != null)&&($post2 ==null)){
                $collection = $all_products->reject(function($element) use ($post) {
                    return mb_strpos($element->fournisseur['name'], $post) === false;
                });
            }
            elseif (($post != null)&&($post2 !=null)) {
                $collection = $all_products->reject(function($element) use ($post) {
                    return mb_strpos($element->fournisseur['name'], $post) === false;
                });
                $collection = $collection->reject(function($element) use ($post2) {
                    return mb_strpos($element->date_add, $post2) === false;
                });
            }elseif (($post==null)&&($post2!=null)) {
                $collection = $all_products->reject(function($element) use ($post2) {
                    return mb_strpos($element->date_add, $post2) === false;
                });
            }
             

            if($collection)
            { 
 
                foreach ($collection as $product) {
 
                $output.='<tr>'.
 
                '<td>'.$product->fournisseur['name'].'</td>'.

                '<td>'.$product->name.'</td>'.
 
                '<td>'.$product->prix_unitaire.'</td>'.
 
                '<td>'.$product->quantité.'</td>'.
 
                '<td>'.$product->total.'</td>'.

                '<td>'.$product->type.'</td>'.

                '<td>'.$product->date_add.'</td>'.

                '<td>'.'                        
                                <a href="/dashboard/Vente/'.$product->id.'/edit"'.'class="btn btn-primary btn-xs"><i
                                class="fa fa-pencil"></i></a>
                                
                </td>'.

 
 
                '</tr>';
 
            }}
            else {
                $output.='<bold> Pas transaction  </bold>';

            }
            }

            elseif ($post1=="Vente") {


                $output="";
                $products=Transaction_vente::all();
                foreach ($products as $product) {
                    $fournisseur=$product->client;
                    $product->push($fournisseur);
                }
                $post=$request->search;
                $post2=$request->search3;
                $collection = collect([]);
                if(($post != null)&&($post2 ==null)){
                    $collection = $products->reject(function($element) use ($post) {
                    return mb_strpos($element->client['name'], $post) === false;
                });
                }elseif(($post != null)&&($post2 !=null)) {
                    $collection = $products->reject(function($element) use ($post) {
                        return mb_strpos($element->client['name'], $post) === false;
                    });
                    $collection = $collection->reject(function($element) use ($post2) {
                        return mb_strpos($element->date_add, $post2) === false;
                    });
                }elseif (($post==null)&&($post2!=null)) {
                    $collection = $products->reject(function($element) use ($post2) {
                        return mb_strpos($element->date_add, $post2) === false;
                    });
                }
             

            if($collection)
            { 
                $total=0; 
                foreach ($collection as $product) {
                $total=$product->total+$total;
                $output.='<tr>'.
 
                '<td>'.$product->client['name'].'</td>'.

                '<td>'.$product->name.'</td>'.
 
                '<td>'.$product->prix_unitaire.'</td>'.
 
                '<td>'.$product->quantité.'</td>'.
 
                '<td>'.$product->total.'</td>'.

                '<td>'.$product->type.'</td>'.

                '<td>'.$product->date_add.'</td>'.

                '<td>'.'                        
                                <a href="/dashboard/achatdiver/'.$product->id.'"class="btn btn-primary btn-xs"><i
                                class="fa fa-pencil"></i></a>


                </td>'.


 
 
                '</tr>';
 
            }
        $output=$output.''.'<tr>'.
 
                '<td>'.'</td>'.

                '<td>'.'</td>'.
 
                '<td>'.'</td>'.
 
                '<td>'."<b>total :</b>".'</td>'.
 
                '<td><b>'.$total.'</b></td>'.

                '<td>'.'</td>'.

                '<td>'.'</td>'.

                '</tr>';

        
        }
            else {
                $output.='<bold> Pas transaction  </bold>';

            }
 
                    # code...
                }
            
        	return Response($output);
 			
 
		}

	}

    public function puttozero(Request $request , $id)
    {
    	# code...
    	/*$test = produit::query()->update([
    		'quantité'=>0
    		]);*/
          
    	produit::where('quantité', '>', 0)->update([
    		'total' => 0,
    		'quantité' => 0
    	]);
        /*dagui*/

        $produit_vente=produit::whereType('achat_divers')->get();
        
        foreach ($produit_vente as $pv) {
            $produit=produit_vente::create([
                'name'=> $pv->name,
                'dimension'=> $pv->mesure,
                'prix_unitaire'=>0 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"disponible",
                'type'=>"vente_normal"
            ]);
        }
        produit::whereType('achat_divers')->delete();
        produit::whereType('achat_divers_divers')->delete();
    	$produit = produit::all();
        $total_prix= produit::all()->sum('total');
        $test=fournisseur::find($id);
        return redirect()->route('achats.index');

    }

    public function puttozer1(Request $request , $id)
    {
        # code...
        /*$test = produit::query()->update([
            'quantité'=>0
            ]);*/
        produit::where('quantité', '>', 0)->update([
            'total' => 0,
            'quantité' => 0
        ]);
        produit::whereType('achat_divers_divers')->delete();
        $produit = produit::all();
        $total_prix= produit::all()->sum('total');
        $test=fournisseur::find($id);
        return redirect()->route('achats.index');

    }
    public function puttozer(Request $request , $id)
    {
    	# code...
    	/*$test = produit::query()->update([
    		'quantité'=>0
    		]);*/
        /*here the function where delete divers achats*/ 
    	produit_vente::where('quantité', '>', 0)->update([
            'total' => 0,
            'quantité' => 0
        ]);
        produit_vente::where('type','=','vente_divers')->delete();
    	$produit = produit_vente::all();
        $total_prix= produit_vente::all()->sum('total');
        $test=Client::find($id);

        return redirect()->route('Vente.index');
    	
    	/*$plucked = $test->pluck('quantité');
    	$multiplied = $plucked->map(function ($item, $key) {
    	return $item * 0;
		});
		*/

    }

    public function showcaisse()
    {
    	$date=NOW()->format('Y-m-d');
        $caisse=Caisse::whereDate_caisse($date)->first();
        if ($caisse!=null) {

            return view('dashboard.Caisse.index',compact('caisse'));
        }else{

            return view('dashboard.Caisse.index');

        }

    }

    public function imprimer(Request $request)
    {
        
    }

    public function add_produit_divers(Request $request,$id)
    {
        /*achats_divers type=1*/
        $date=NOW()->format('Y-m-d');
        $total=$request->quantite * $request->prix_unitaire;
        $trans=transaction_achats_divers::create([
            'prix_unitaire'=> $request->prix_unitaire,
            'quantité'=> $request->quantite,
            'total'=> $total,
            'type'=> 1,
            'name'=> $request->name,
            'date_add'=> $date,
            'fournisseur_id'=>$id,
            'mesure'=>$request->mesure,
            'produit_id'=>0
        ]);
        $last_caisse=Caisse::where( 'date_caisse', '=' , $date)
           ->first();
        if ($last_caisse==null) {
               $total_achat_divers=transaction_achats_divers::whereDate_add($date)->sum("total");
               Caisse::create([
                    'total_vente'=>0,
                    'total_achat'=>0,
                    'total_achat_divers'=>$total_achat_divers,
                    'caisse'=>0,
                    'date_caisse'=>$date
               ]);
         }else{
            
            $total_achat_divers=transaction_achats_divers::whereDate_add($date)->sum("total");
            $last_caisse=Caisse::where( 'date_caisse', '=' , $date)->first();
            $last_caisse->update([
               'total_achat_divers'=> $total_achat_divers 
            ]);
         }
        $date=NOW()->format('Y-m-d');
        $produit=produit::create([
            'name'=> $request->name,
            'mesure'=> $request->mesure,
            'prix_unitaire'=> $request->prix_unitaire,
            'quantité'=> $request->quantite,
            'total'=>$total,
             'fournisseur_id'=>$id,
            'transaction_id'=>$trans->id,
            'type'=>"achat_divers_divers"
        ]);
        
        $total= produit::all()->sum('total');
        return response()->json([$produit,$total]);
   

    }


  public function get_achats($id)
  {
        $test = Client::find($id);
        if($test->type=="Normal")
            $produit=produit_vente::all();
        else if ($test->type=="particulier") {
            $produit=produit_vente_particulier::all();
            dd($produit);
        } 

        $test = Client::find($id);
        $credit=credit::whereId_client($test->id)->first();
        return response()->json([$produit,$credit],200);
      # code...
  }
  
public function add_achats($id,Request $request){

    $date=NOW()->format('Y-m-d');
    $total=$request->quantite*$request->prix_unitaire;
    /*type=1*/
    $trans=transaction_vente::create([
        'prix_unitaire'=> $request->prix_unitaire,
        'quantité'=> $request->quantite,
        'total'=> $total,
        'type'=> 1,
        'name'=> $request->name,
        'date_add'=> $date,
        'dimension'=>$request->dimension,
        'client_id'=>$id,
        'dispo'=>"disponible"
    ]);
    $last_caisse=Caisse::where( 'date_caisse', '=' , $date)
           ->first();
    if ($last_caisse==null) {
               $total_achat_divers=transaction_vente::whereDate_add($date)->sum("total");
               Caisse::create([
                    'total_vente'=>$total_achat_divers,
                    'total_achat'=>0,
                    'total_achat_divers'=>0,
                    'caisse'=>0,
                    'date_caisse'=>$date
               ]);
         }else{
            
            $total_achat_divers=transaction_vente::whereDate_add($date)->sum("total");
            $last_caisse=Caisse::where( 'date_caisse', '=' , $date)->first();
            $last_caisse->update([
               'total_vente'=>$total_achat_divers 
            ]);
    }
    $date=NOW()->format('Y-m-d');
    $produit=produit_vente::create([
        'name'=> $request->name,
        'dimension'=> $request->dimension,
        'prix_unitaire'=> $request->prix_unitaire,
        'quantité'=> $request->quantite,
        'total'=>$total,
        'client_id'=>$id,
        'transaction_id'=>$trans->id,
        'dispo'=>"disponible",
        'type'=>"vente_divers"
    ]);
    $total= produit_vente::all()->sum('total');
    return response()->json([$produit,$total]);
 }
  
public function upd_achats(Request $request){

    $date=NOW()->format('Y-m-d');
    $trans=transaction_vente::whereId($request->transaction_id)->first();
        
    if ($trans==null) {
        $trans=transaction_vente::create([
        'prix_unitaire'=> $request->prix_unitaire,
        'quantité'=> $request->quantité,
        'total'=> $request->prix_unitaire*$request->quantité,
        'dimension'=> $request->dimension,
        'name'=> $request->name,
        'date_add'=> $date,
        'dispo'=>"disponible",
        'client_id'=>$request->client_id
        ]);
    }else 
    $trans->update([
        'prix_unitaire'=> $request->prix_unitaire,
        'quantité'=> $request->quantité,
        'total'=> $request->prix_unitaire*$request->quantité,
        'dimension'=> $request->dimension,
        'name'=> $request->name,
        'date_add'=> $date
    ]);

    $last_caisse=Caisse::where( 'date_caisse', '=' , $date)
           ->first();
    if ($last_caisse==null) {
               $total_achat_divers=transaction_vente::whereDate_add($date)->sum("total");
               Caisse::create([
                    'total_vente'=>$total_achat_divers,
                    'total_achat'=>0,
                    'total_achat_divers'=>0,
                    'caisse'=>0,
                    'date_caisse'=>$date
               ]);
         }else{
            
            $total_achat_divers=transaction_vente::whereDate_add($date)->sum("total");
            $last_caisse=Caisse::where( 'date_caisse', '=' , $date)->first();
            $last_caisse->update([
               'total_vente'=>$total_achat_divers 
            ]);
    }
    $produit1=produit_vente::find($request->id);
    $produit1->update([
        "name" => $request->name,
        "mesure" => $request->mesure,
        "prix_unitaire" => $request->prix_unitaire,
        "quantité" => $request->quantité,
        "total"=>$request->prix_unitaire*$request->quantité,
        "transaction_id"=>$trans->id
    ]);
    $produit=produit_vente::all();
    return response()->json($produit);
} 

}	
