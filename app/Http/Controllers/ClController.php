<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\credit;
use View;
class ClController extends Controller
{
    //
    public function index()
    {
    	$credit=credit::whereType("client")->get();
        $userData=[];
        $i=0;
        $sum_credit=0;
        $progression=0;
    	foreach ($credit as $cr) {
    			/*$fournisseur=$cr->fournisseur;
    			$latestop=$fournisseur->Transaction_achats;
    			$latestop1=$fournisseur->Transaction_achats_divers;

				if(($latestop->count() <=0) OR ($latestop1->count()<=0)) continue;
    			$sorted = $latestop->sortByDesc(function($post)
				{
  					return $post->created_at;
				});

				$sorted1= $latestop1->sortByDesc(function($post)
				{
  					return $post->created_at;
				});
				$taille=$sorted->count()-1;
                  
				$taille1=$sorted1->count()-1;

                if($sorted1[$taille1]['date_add']<$sorted[$taille]['date_add']){
					$cr->push($sorted[$taille]);
					$userData[] = [
                    'date' => $sorted[$taille],
                    'cr' => $cr,
                	];
				}
				else {
					$cr->push($sorted1[$taille1]);
					$userData[] = [
                    'date' => $sorted1[$taille1],
                    'cr' => $cr,
                	];
				}

				

    			//$cr->push($fournisseur);
    		}else {*/
    			$fournisseur=$cr->client;
                if ($fournisseur==null) {
                    # code...
                    continue;
                }
                $latestop=$fournisseur->Transaction_ventes;
    			if(($latestop->count()==0)) 
                { 
                    $userData[$i] = [
                            'date' => '',
                            'cr' => $cr,
                        ];
                    $i++;
                    $sum_credit=$sum_credit+$cr->montant_credit;
                    $progression=$progression+$cr->progression;

                    continue;
                    
                }
    			$sorted = $latestop->sortByDesc(function($post)
				{
  					return $post->created_at;
				});
				$taille=$sorted->count()-1;
				$userData[$i] = [
                    'date' => $sorted[$taille],
                    'cr' => $cr,
                ];
                $sum_credit=$sum_credit+$cr->montant_credit;
                $progression=$progression+$cr->progression;

                $i++;
				
				
    		 
    	}
         return view('dashboard.cl.index',compact('userData',$userData,'sum_credit',$sum_credit,'progression',$progression));
    	

    }
}
