<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use View;
use App\produit_vente;
use App\credit;

class VenteController extends Controller
{
    //

    //
    public function imprimer(Request $request)
    {
        $produit = produit_vente::where('total','!=',0)->get();
        dd($produit);
        $total_prix= produit_vente::all()->sum('total');
    }



      public function index()
    {
        // load the view and pass the tests

        $tests = Client::all();
        if (request()->wantsJson()) {
            return response()->json($tests);
                                    }
        return View::make('dashboard.Vente.index')->with('tests', $tests);;
    }

    public function cherchez(Request $request)
    {
      $output="";
            $products=client::where('name','LIKE','%'.$request->search."%")->get();
            if($products)
            {
 
                foreach ($products as $key => $product) {
 
                $output.='<tr>'.
 
                '<td>'.$product->name.'</td>'.
 
                '<td>'.$product->adresse.'</td>'.
 
                '<td>'.$product->Tel.'</td>'.

                '<td>'.'                        
                                <a href="/dashboard/Vente/'.$product->id.'/edit"'.'class="btn btn-primary btn-xs"><i
                                class="fa fa-pencil"></i></a>
                                

                     
                                <form method="POST" action="/dashboard/Vente/'.$product->id.'  style="display:inline"><input name="_method" type="hidden" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button type="submit" class="btn btn-danger btn-xs cur-p"><i class="fa      fa-trash"></i></button>
                                </form>
                     
                </td>'.
            
                '</tr>';
 
            }
            return Response($output);
            }

        # code...
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('dashboard.Vente.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        //$this->validate($request,fournisseur::rules());
         
        $test = Client::create(($request->all()));
        $credit=credit::create([
                'type'=>"client",
                'montant_credit'=>0,
                'id_client'=>$test->id,
                'progression'=>0,
                'tour'=>0
        ]);

        return redirect()->route('Vente.index'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    /*public function show($id)
    {
        return Test::with('quizes')->findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $test = client::find($id);
        $produit = produit_vente::all();
        $total_prix= produit_vente::all()->sum('total');
        return view('dashboard.Vente.edit',compact('test','produit','total_prix'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,Test $test)
    {
        $this->validate($request,fournisseur::rules(true));
        $test->update($request->all());
        return redirect()->back()->withSuccess(trans('app.success_update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {

        Client::destroy($id);
        return redirect()->route('Vente.index');
    } 
}
