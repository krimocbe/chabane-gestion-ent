<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\produit;
use App\fournisseur;
use App\Transaction_achat;
use App\Transaction_vente;
use App\credit;
use App\Caisse;

use View;

class produitController extends Controller
{
    //
      public function index()
    {
        // load the view and pass the tests

        $tests = fournisseur::all();
        if (request()->wantsJson()) {
            return response()->json($tests);
                                    }
        return View::make('dashboard.achats.index')->with('tests', $tests);;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('dashboard.achats.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        //$this->validate($request,fournisseur::rules());
        
        $transaction=Transaction_achat::create(($request->all()));
        $test = fournisseur::create(($request->all()));
        return redirect()->route('achats.edit',$test->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    /*public function show($id)
    {
        return Test::with('quizes')->findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $test = produit::where('id', $id)->first();
        
        return view('dashboard.achats.editproduit',compact('test'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        
        $tr=Transaction_vente::find($id)->first();
        $tr1=Transaction_vente::find($id);
        $tr1->update([
        	'prix_unitaire'=> $request->prix_unitaire,
        	'quantité'=> $request->quantité,
        	'total'=> $request->quantité*$request->prix_unitaire,
        	'dimension'=> $request->mesure,
        	'name'=> $request->name
		]);
        return view('dashboard.achats.edit',compact('test','produit','total_prix','montant')); 
		    
	}

	public function showtotal()
	{
		dd("shit");
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {

        fournisseur::destroy($id);

        return redirect()->route('tests.index');
    }  
}
