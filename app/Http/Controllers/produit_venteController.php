<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\produit_vente;
use App\Transaction_vente;

class produit_venteController extends Controller
{
    //
    public function edit($id)
    {
        $test = produit_vente::where('id', $id)->first();
        
        return view('dashboard.Vente.editproduit',compact('test'));
    }

        public function update(Request $request,$id)
    {
        $test = produit_vente::where('id', $id)->first();
        $total =  $request->prix_unitaire*$request->quantité;
        $test->update([
        	'prix_unitaire'=> $request->prix_unitaire,
        	'quantité'=> $request->quantité,
        	'total'=> $total
    			]);
		$date=NOW();	
        Transaction_vente::create([
        	'prix_unitaire'=> $request->prix_unitaire,
        	'quantité'=> $request->quantité,
        	'total'=> $total,
        	'dimension'=> $request->dimension,
        	'name'=> $request->name,
        	'client_id'=>$test->client_id,
        	'dispo'=>$test->dispo,
        	'date_add'=> $date
		]);
        $produit = produit_vente::all();
        $total_prix= produit_vente::all()->sum('total');
        return view('dashboard.Vente.edit',compact('produit','total_prix','test')); 
		    
	}

}
