<?php

use Illuminate\Database\Seeder;
use App\produit;
use App\produit_vente;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $produit=produit::create([
            'name'=>"Mousse Molle",
            'mesure'=> "120*60*10",
            'prix_unitaire'=> 520,
            'quantité'=> 0,
            'total'=>0,
            'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);
        $produit=produit::create([
            'name'=>"Mousse Molle",
            'mesure'=> "120*65*10",
            'prix_unitaire'=> 800,
            'quantité'=> 0,
            'total'=>0,
             'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);
        $produit=produit::create([
            'name'=>"Mousse Molle",
            'mesure'=> "120*65*12",
            'prix_unitaire'=> 960,
            'quantité'=> 0,
            'total'=>0,
             'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);
        $produit=produit::create([
            'name'=>"Mousse Molle",
            'mesure'=> "120*65*14",
            'prix_unitaire'=> 1120,
            'quantité'=> 0,
            'total'=>0,
             'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);
        $produit=produit::create([
            'name'=>"Mousse Molle",
            'mesure'=> "120*65*16",
            'prix_unitaire'=> 1280,
            'quantité'=> 0,
            'total'=>0,
             'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);
        $produit=produit::create([
            'name'=>"Mousse Molle",
            'mesure'=> "120*65*18",
            'prix_unitaire'=> 1440,
            'quantité'=> 0,
            'total'=>0,
             'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);
        $produit=produit::create([
            'name'=>"Mousse Molle",
            'mesure'=> "120*65*20",
            'prix_unitaire'=> 1600,
            'quantité'=> 0,
            'total'=>0,
             'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);
        $produit=produit::create([
            'name'=>"Mousse Molle",
            'mesure'=> "120*80*16",
            'prix_unitaire'=> 1700,
            'quantité'=> 0,
            'total'=>0,
             'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);
        $produit=produit::create([
            'name'=>"Mousse Molle",
            'mesure'=> "120*80*20",
            'prix_unitaire'=> 2200,
            'quantité'=> 0,
            'total'=>0,
             'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);
        $produit=produit::create([
            'name'=>"Mousse Molle",
            'mesure'=> "190*140*20",
            'prix_unitaire'=> 3500,
            'quantité'=> 0,
            'total'=>0,
             'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);
        $produit=produit::create([
            'name'=>"Mousse Demi-Dure",
            'mesure'=> "180*65*16",
            'prix_unitaire'=> 1932,
            'quantité'=> 0,
            'total'=>0,
             'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);
        $produit=produit::create([
            'name'=>"Mousse Demi-Dure",
            'mesure'=> "120*60*14",
            'prix_unitaire'=> 1114.35,
            'quantité'=> 0,
            'total'=>0,
             'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);
        $produit=produit::create([
            'name'=>"Mousse Dure",
            'mesure'=> "180*65*16",
            'prix_unitaire'=> 2600,
            'quantité'=> 0,
            'total'=>0,
             'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);
        $produit=produit::create([
            'name'=>"Mousse Dure D face",
            'mesure'=> "180*65*18",
            'prix_unitaire'=> 2929,
            'quantité'=> 0,
            'total'=>0,
             'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);
        $produit=produit::create([
            'name'=>"Mousse Dure D face",
            'mesure'=> "190*70*18",
            'prix_unitaire'=> 3330,
            'quantité'=> 0,
            'total'=>0,
             'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);
        $produit=produit::create([
            'name'=>"Mousse Dure Une face",
            'mesure'=> "190*80*18",
            'prix_unitaire'=> 3800,
            'quantité'=> 0,
            'total'=>0,
             'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);
        $produit=produit::create([
            'name'=>"Mousse Dure D face",
            'mesure'=> "190*80*20",
            'prix_unitaire'=> 4335.50,
            'quantité'=> 0,
            'total'=>0,
             'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);
        $produit=produit::create([
            'name'=>"Mousse Dure D face",
            'mesure'=> "190*140*20",
            'prix_unitaire'=> 7590,
            'quantité'=> 0,
            'total'=>0,
             'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);

        $produit=produit::create([
            'name'=>"Mousse Densité 30",
            'mesure'=> "180*65*18",
            'prix_unitaire'=> 3602,
            'quantité'=> 0,
            'total'=>0,
             'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);

        $produit=produit::create([
            'name'=>"Mousse Densité 30",
            'mesure'=> "190*70*18",
            'prix_unitaire'=> 4095,
            'quantité'=> 0,
            'total'=>0,
             'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);
        $produit=produit::create([
            'name'=>"Mousse Densité 30",
            'mesure'=> "190*80*18",
            'prix_unitaire'=> 4860,
            'quantité'=> 0,
            'total'=>0,
             'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);
        $produit=produit::create([
            'name'=>"Mousse Densité 30",
            'mesure'=> "190*90*18",
            'prix_unitaire'=> 5265,
            'quantité'=> 0,
            'total'=>0,
             'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);
        $produit=produit::create([
            'name'=>"Mousse Densité 30",
            'mesure'=> "190*140*18",
            'prix_unitaire'=> 8190,
            'quantité'=> 0,
            'total'=>0,
             'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);
        $produit=produit::create([
            'name'=>"Mousse Densité 30",
            'mesure'=> "190*140*23",
            'prix_unitaire'=> 10465,
            'quantité'=> 0,
            'total'=>0,
             'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);
        $produit=produit::create([
            'name'=>"Mousse Densité 30",
            'mesure'=> "190*140*28",
            'prix_unitaire'=> 12740,
            'quantité'=> 0,
            'total'=>0,
             'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);
        $produit=produit::create([
            'name'=>"Mousse Densité 30",
            'mesure'=> "190*160*18",
            'prix_unitaire'=> 9360,
            'quantité'=> 0,
            'total'=>0,
             'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);
        $produit=produit::create([
            'name'=>"Mousse Densité 30",
            'mesure'=> "190*160*23",
            'prix_unitaire'=> 11960,
            'quantité'=> 0,
            'total'=>0,
             'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);

        $produit=produit::create([
            'name'=>"Mousse Densité 30",
            'mesure'=> "200*180*18",
            'prix_unitaire'=> 11000,
            'quantité'=> 0,
            'total'=>0,
             'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);
        $produit=produit::create([
            'name'=>"Mousse Densité 30",
            'mesure'=> "200*180*23",
            'prix_unitaire'=> 14100,
            'quantité'=> 0,
            'total'=>0,
             'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);
        $produit=produit::create([
            'name'=>"Mousse Densité 30",
            'mesure'=> "160*28",
            'prix_unitaire'=> 14560,
            'quantité'=> 0,
            'total'=>0,
             'fournisseur_id'=>1,
            'type'=>"achat_normal"
        ]);


        /*vente direct*/
        $produit=produit_vente::create([
                'name'=> "Mousse Molle",
                'dimension'=> "120*60*10",
                'prix_unitaire'=>1000 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"disponible",
                'type'=>"vente_normal"
            ]);
        $produit=produit_vente::create([
                'name'=> "Mousse Molle",
                'dimension'=> "180*65*10",
                'prix_unitaire'=>1500 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"Par commande",
                'type'=>"vente_normal"
            ]);
        $produit=produit_vente::create([
                'name'=> "Mousse Molle",
                'dimension'=> "180*65*12",
                'prix_unitaire'=>1600 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"Par commande",
                'type'=>"vente_normal"
            ]);
        $produit=produit_vente::create([
                'name'=> "Mousse Molle",
                'dimension'=> "180*65*14",
                'prix_unitaire'=>1700 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"Disponible",
                'type'=>"vente_normal"
            ]);
        $produit=produit_vente::create([
                'name'=> "Mousse Molle",
                'dimension'=> "180*65*16",
                'prix_unitaire'=>1800 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"Disponible",
                'type'=>"vente_normal"
            ]);
        $produit=produit_vente::create([
                'name'=> "Mousse Molle",
                'dimension'=> "180*65*18",
                'prix_unitaire'=>1900 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"Disponible",
                'type'=>"vente_normal"
            ]);
        $produit=produit_vente::create([
                'name'=> "Mousse Molle",
                'dimension'=> "180*65*20",
                'prix_unitaire'=>2000 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"Disponible",
                'type'=>"vente_normal"
            ]);
        $produit=produit_vente::create([
                'name'=> "Mousse Molle",
                'dimension'=> "180*80*16",
                'prix_unitaire'=>2450 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"Disponible",
                'type'=>"vente_normal"
            ]);
        $produit=produit_vente::create([
                'name'=> "Mousse Molle",
                'dimension'=> "180*80*20",
                'prix_unitaire'=>2750 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"Disponible",
                'type'=>"vente_normal"
            ]);
        $produit=produit_vente::create([
                'name'=> "Mousse Molle",
                'dimension'=> "180*140*20",
                'prix_unitaire'=>4200 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"Disponible",
                'type'=>"vente_normal"
            ]);
        $produit=produit_vente::create([
                'name'=> "Mousse Demi-Dure",
                'dimension'=> "180*65*16",
                'prix_unitaire'=>2600 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"Disponible",
                'type'=>"vente_normal"
            ]);
        $produit=produit_vente::create([
                'name'=> "Mousse Demi-Dure",
                'dimension'=> "180*60*14",
                'prix_unitaire'=>2500 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"Disponible",
                'type'=>"vente_normal"
            ]);
        $produit=produit_vente::create([
                'name'=> "Mousse Dure",
                'dimension'=> "180*65*16",
                'prix_unitaire'=>3400 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"Disponible",
                'type'=>"vente_normal"
            ]);
        $produit=produit_vente::create([
                'name'=> "Mousse Dure D face",
                'dimension'=> "180*65*18",
                'prix_unitaire'=>4800 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"Disponible",
                'type'=>"vente_normal"
            ]);
        $produit=produit_vente::create([
                'name'=> "Mousse Dure D face",
                'dimension'=> "180*70*18",
                'prix_unitaire'=>5400 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"Disponible",
                'type'=>"vente_normal"
            ]);
        $produit=produit_vente::create([
                'name'=> "Mousse Dure Une face",
                'dimension'=> "180*80*18",
                'prix_unitaire'=>6000 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"Disponible",
                'type'=>"vente_normal"
            ]);
        $produit=produit_vente::create([
                'name'=> "Mousse Dure D face",
                'dimension'=> "180*80*20",
                'prix_unitaire'=>6300 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"Disponible",
                'type'=>"vente_normal"
            ]);
        $produit=produit_vente::create([
                'name'=> "Mousse Dure D face",
                'dimension'=> "190*140*20",
                'prix_unitaire'=>10500 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"Disponible",
                'type'=>"vente_normal"
            ]);
        $produit=produit_vente::create([
                'name'=> "Mousse Densité 30",
                'dimension'=> "180*65*20",
                'prix_unitaire'=>5200 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"Disponible",
                'type'=>"vente_normal"
            ]);
        $produit=produit_vente::create([
                'name'=> "Mousse Densité 30",
                'dimension'=> "190*70*20",
                'prix_unitaire'=>5800 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"Disponible",
                'type'=>"vente_normal"
            ]);
        $produit=produit_vente::create([
                'name'=> "Mousse Densité 30",
                'dimension'=> "190*80*20",
                'prix_unitaire'=>6500 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"Disponible",
                'type'=>"vente_normal"
            ]);
        $produit=produit_vente::create([
                'name'=> "Mousse Densité 30",
                'dimension'=> "190*90*20",
                'prix_unitaire'=>7000 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"Disponible",
                'type'=>"vente_normal"
            ]);
        $produit=produit_vente::create([
                'name'=> "Mousse Densité 30",
                'dimension'=> "190*140*20",
                'prix_unitaire'=>11000 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"Disponible",
                'type'=>"vente_normal"
            ]);
        $produit=produit_vente::create([
                'name'=> "Mousse Densité 30",
                'dimension'=> "190*140*25",
                'prix_unitaire'=>14000 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"Disponible",
                'type'=>"vente_normal"
            ]);
        $produit=produit_vente::create([
                'name'=> "Mousse Densité 30",
                'dimension'=> "190*140*30",
                'prix_unitaire'=>16000 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"Par Commande",
                'type'=>"vente_normal"
            ]);
        $produit=produit_vente::create([
                'name'=> "Mousse Densité 30",
                'dimension'=> "190*160*20",
                'prix_unitaire'=>12800 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"Disponible",
                'type'=>"vente_normal"
            ]);
        $produit=produit_vente::create([
                'name'=> "Mousse Densité 30",
                'dimension'=> "190*160*25",
                'prix_unitaire'=>16000 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"Disponible",
                'type'=>"vente_normal"
            ]);
        $produit=produit_vente::create([
                'name'=> "Mousse Densité 30",
                'dimension'=> "190*160*30",
                'prix_unitaire'=>18500 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"Par Commande",
                'type'=>"vente_normal"
            ]);
        $produit=produit_vente::create([
                'name'=> "Mousse Densité 30",
                'dimension'=> "200*180*20",
                'prix_unitaire'=>15700 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"Par Commande",
                'type'=>"vente_normal"
            ]);
        $produit=produit_vente::create([
                'name'=> "Mousse Densité 30",
                'dimension'=> "200*180*25",
                'prix_unitaire'=>19500 ,
                'quantité'=> 0,
                'total'=>0,
                'dispo'=>"Par Commande",
                'type'=>"vente_normal"
            ]);
    }
}
