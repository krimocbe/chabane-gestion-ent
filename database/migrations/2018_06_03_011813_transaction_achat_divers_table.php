<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransactionAchatDiversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('transaction-achats-divers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('mesure');
            $table->integer('produit_id')->default(0);
            $table->integer('type');
            $table->integer('prix_unitaire');
            $table->integer('quantité');
            $table->integer('total');
            $table->integer('fournisseur_id')->unsigned();
            $table->date('date_add');
            $table->integer('afficher')->default(0);
            $table->foreign('fournisseur_id')->references('id')->on('fournisseur')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
