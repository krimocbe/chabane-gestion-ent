<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionVenteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_vente', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('dimension');
            $table->string('dispo');
            $table->integer('prix_unitaire');
            $table->integer('quantité');
            $table->integer('total');
            $table->date('date_add');
            $table->integer('client_id')->unsigned();
            $table->foreign('client_id')->references('id')->on('client')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_vente');
    }
}
