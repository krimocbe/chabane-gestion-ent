<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaisseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('caisse', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date_caisse');
            $table->integer('total_vente');
            $table->integer('total_achat');
            $table->integer('total_achat_divers');
            $table->integer('caisse');
            $table->integer('derniere_caisse');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('caisse');
    }
}
