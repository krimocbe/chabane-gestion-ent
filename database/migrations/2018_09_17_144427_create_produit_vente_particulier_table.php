<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProduitVenteParticulierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produit_vente_particulier', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('dimension');
            $table->string('dispo');
            $table->integer('prix_unitaire');
            $table->integer('quantité');
            $table->integer('total');
            $table->string('type');
            $table->integer('transaction_id')->default(0);
            $table->integer('client_id')->unsigned()->default(1);
            $table->timestamps();
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produit_vente_particulier');
    }
}
