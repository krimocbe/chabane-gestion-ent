<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransactionAchatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('transaction-achats', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('mesure');
            $table->string('type')->default("achat_normal");
            $table->integer('prix_unitaire');
            $table->integer('quantité');
            $table->integer('total');
            $table->date('date_add');
            $table->integer('produit_id')->default(0);
            $table->integer('fournisseur_id')->unsigned();
            $table->foreign('fournisseur_id')->references('id')->on('fournisseur')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
