<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('montant_credit');
            $table->string('type');
            $table->integer('id_client')->unsigned()->nullable();
            $table->foreign('id_client')->references('id')->on('client')->onDelete('cascade');
            $table->integer('id_fournisseur')->unsigned()->nullable();
            $table->foreign('id_fournisseur')->references('id')->on('fournisseur')->onDelete('cascade');

            $table->integer('progression')->default(0);
            $table->integer('tour')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credits');
    }
}
